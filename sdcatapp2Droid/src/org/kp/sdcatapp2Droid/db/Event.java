package org.kp.sdcatapp2Droid.db;

import java.security.InvalidParameterException;
import java.util.Date;


public class Event {
	public final static int CHECKING_IN_EVENT = 0;
	public final static int CHECKING_OUT_EVENT = 1;
	public final static int MEAL_EVENT = 2;
	public final static int CHECK_UP_EVENT = 3;
	public final static int DOCTOR_VISIT = 4;
	public final static int MEDICATION_EVENT = 5;
	public final static int BLOOD_TEST_EVENT = 6;
	public final static int VISITING_EVENT = 7;
	public final static int HYGIENE_EVENT = 8;
	public final static int SURGERY_EVENT = 9;
	
	private String mName;
	private String mInvolved;
	private String mNotes;
	private int mType;

	// start and end times of this event
	private Date mStartTime;
	private double mDuration;

	// review of this event
	private Feedback mReview;
	
	public Event(String name, int type, String involved, Date start, double dur){
		if(name == null){
			throw new NullPointerException("Event can't have null name.");
		}
		else if (start == null){
			throw new NullPointerException("Start time can't be null.");
		}
		else if (dur <= 0){
			throw new InvalidParameterException("Duration has to be a positive value.");
		}
		
		mName = name;
		mInvolved = involved;
		mType = type;
		mStartTime = start;
		mDuration = dur;
		mNotes = "";
	}

	/**
	 * Get name of this event.
	 */
	public String getName(){
		return mName;
	}

	/**
	 * Get name of hospital staff involved with this event.
	 * @return Name of hospital staff.
	 */
	public String getPersonInvolved(){
		return mInvolved;
	}
	
	public String getNotes(){
		return mNotes;
	}
	
	public void setNotes(String notes){
		mNotes = notes;
	}

	/**
	 * Numeric representation of category this event falls in. Should
	 * be used for determining icons.
	 */
	public int getType(){
		return mType;
	}

	/**
	 * Gets string representation of start time HH:mm.
	 * @return String representation of time.
	 */
	public String getTimeStr(){
		//EEE MMM dd HH:mm:ss 222
		String[] dateSplit = mStartTime.toString().split(" ");
		return dateSplit[3].substring(0,5);
	}
	
	/**
	 * Get duration in hours. Ex. "0.5 hr".
	 * @return Hours in str.
	 */
	public String getDuration(){
		return String.valueOf(mDuration) + " hr";
	}

	/**
	 * Gets string representation of event's date: MMM DD YYYY
	 * @return String representation of date.
	 */
	public String getDateStr(){
		StringBuilder ret = new StringBuilder();
		
		//EEE MMM dd HH:mm:ss 222
		String[] dateSplit = mStartTime.toString().split(" ");
		ret.append(dateSplit[1]); // month
		ret.append(" ");
		ret.append(dateSplit[2]); // day
		ret.append(" ");
		ret.append(dateSplit[5]); // year
		
		return ret.toString();
	}

	/**
	 * Set feedback.
	 * @param rating Number representing rating.
	 * @param details Details of rating.
	 */
	public void setFeedback(int rating, String details){
		if(mReview == null){
			mReview = new Feedback(rating, details);
			return;
		}
		
		mReview.setFeedback(rating, details);
	}

	/**
	 * Gets feedback of this event. If no feedback has been
	 * provided, returns null.
	 * @return Feedback object, or null if no feedback provided.
	 */
	public Feedback getFeedback(){
		return mReview;
	}

}
