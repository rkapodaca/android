package com.Ripped.Control;

import java.util.ArrayList;
import java.util.Random;

import android.content.Context;

import com.Ripped.Model.CustomWodTable;
import com.Ripped.Model.ExerTable;

public class WodGen {
	private static ArrayList<String> exercises;
	private static String typeWod;
	private static Context context;
	
	

	/**
	 * @param args
	 */
	public static boolean generate(Context ctx, boolean wFilt, boolean eFilt, boolean uFilt, 
								boolean lowFilt, boolean largeFilt) {
			// TODO Auto-generated method stub
		ExerTable db = new ExerTable(ctx);
		context = ctx;
		int w = 1,  e= 1, u= 0, low= 0, large= 0;
		
		if(wFilt) w = 0;
		if(eFilt) e = 0;
		if(uFilt) u = 1;
		if(lowFilt) low = 1;
		if(largeFilt) large = 1;

		
		String[] exers = db.getExercise(w, e, u, low, large);
		
		System.out.println(exers.length);
		if(exers.length < 5)
			return false;
			
	    Random rndNum = new Random();
	    int numExercises = rndNum.nextInt(4) + 2;
	    
	    int type = rndNum.nextInt(2);
	    
	    if (type == 0)
	    {
	    	int rounds = rndNum.nextInt(8) + 3;
	    	typeWod = "For time " + rounds + " rounds";
	    }
	    else 
	    {
	    	int time = (rndNum.nextInt(3) + 1) * 5;
	    	typeWod = "AMRAP in " + time + " mins";
	    }
	    
	    exercises = new ArrayList<String>(numExercises);
	    
	    int num = 0;
	    for (int i = 0; i < numExercises; i++ )
	    {
	    	
	    	num = rndNum.nextInt(exers.length);
	    	while (exercises.contains(exers[num]))
	    		num = rndNum.nextInt(exers.length);
	    	exercises.add(exers[num]);
	    }

	    return true;
	    
	}
	
	public static ArrayList<String> getWOD()
	{
		
		String wod = typeWod;
		Random rndNum = new Random();
		int reps = 0; 
		String exe = "";
		ArrayList<String> customWodList = new ArrayList<String>();
	
		for(String ex: exercises)
	    {
			if (ex.contains("Sprint") || ex.contains("Run")){
				wod = wod + "\n\t" + ex;
				exe = exe + ", " + ex;
			}
			else {
				reps = 9 + rndNum.nextInt(3) * 6;
			   	wod = wod + "\n\t" + reps + "x " + ex;
			   	if(exe.equals(""))
			   		exe = + reps + "x " + ex;
			   	else
			   		exe = exe + ", " + reps + "x " + ex;
			}
	    }
		customWodList.add(exe);
		customWodList.add(typeWod);
		
		return customWodList;
		
	}
	
	

}
