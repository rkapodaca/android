package com.Ripped.Model.Initialize;

import com.Ripped.Model.ExerTable;

import android.database.sqlite.SQLiteDatabase;

public class InitializeExerciseTable {
			
	public static void initialize(SQLiteDatabase db){

		
			ExerTable.addInitialExercise(db, "Squat(with barbell)",1,1,0,1,0);
            ExerTable.addInitialExercise(db, "Barbell snatch",1,1,0,1,0);
            ExerTable.addInitialExercise(db, "Bar muscle up",1,1,0,1,0);
            ExerTable.addInitialExercise(db, "Clean & Jerk",1,1,1,1,0);
            ExerTable.addInitialExercise(db, "Dumbbell Snatch",1,1,1,0,0);
            ExerTable.addInitialExercise(db, "Dumbbell split lifts",1,1,0,0,0);
            ExerTable.addInitialExercise(db, "Hang Clean",1,1,0,1,0);
            ExerTable.addInitialExercise(db, "Hang Power Clean",1,1,0,1,0);
            ExerTable.addInitialExercise(db, "Hang Power Snatch",1,1,1,1,0);
            ExerTable.addInitialExercise(db, "Hang Snatch",1,1,0,0,0);
            ExerTable.addInitialExercise(db, "Heaving Snatch Balance",1,1,0,0,0);
            ExerTable.addInitialExercise(db, "Jerk",1,1,1,0,0);
            ExerTable.addInitialExercise(db, "Jerk (behind the head)",1,1,1,0,0);
            ExerTable.addInitialExercise(db, "Kettlebell swings",1,1,1,0,0);
            ExerTable.addInitialExercise(db, "Overhead Squat",1,1,1,1,0);
            ExerTable.addInitialExercise(db, "Power Clean",1,1,0,1,0);
            ExerTable.addInitialExercise(db, "Power Snatch",1,1,1,0,0);
            ExerTable.addInitialExercise(db, "Pressing Snatch Balance",1,1,1,0,0);
            ExerTable.addInitialExercise(db, "Push Jerk",1,1,1,0,0);
            ExerTable.addInitialExercise(db, "Push Press",1,1,1,0,0);
            ExerTable.addInitialExercise(db, "Shoulder Press",1,1,1,0,0);
            ExerTable.addInitialExercise(db, "Bench Press",1,1,1,0,0);
            ExerTable.addInitialExercise(db, "Deadlift",1,1,0,1,0);
            ExerTable.addInitialExercise(db, "Kipping Pullups",0,1,0,0,0);
            ExerTable.addInitialExercise(db, "Air Squat",0,0,0,1,0);
            ExerTable.addInitialExercise(db, "Back extension",0,0,0,0,0);
            ExerTable.addInitialExercise(db, "Box Jump Variations",0,1,0,1,0);
            ExerTable.addInitialExercise(db, "Burpee",0,0,0,0,0);
            ExerTable.addInitialExercise(db, "Pushups",0,0,1,0,0);
            ExerTable.addInitialExercise(db, "Knuckle Pushups",0,0,1,0,0);
            ExerTable.addInitialExercise(db, "Wide Pushups",0,0,1,0,0);
            ExerTable.addInitialExercise(db, "Dip",0,0,1,0,0);
            ExerTable.addInitialExercise(db, "Double Unders",0,1,1,0,0);
            ExerTable.addInitialExercise(db, "Glute-Ham Sit-up",0,0,0,0,0);
            ExerTable.addInitialExercise(db, "Handstand Push-up",0,0,1,0,0);
            ExerTable.addInitialExercise(db, "L-sit",0,0,0,0,0);
            ExerTable.addInitialExercise(db, "Medicine Ball Cleans",0,1,1,0,0);
            ExerTable.addInitialExercise(db, "Muscle-up",0,1,1,0,0);
            ExerTable.addInitialExercise(db, "Ring Dips",0,1,1,0,0);
            ExerTable.addInitialExercise(db, "Walking Lunge",0,0,0,1,0);
            ExerTable.addInitialExercise(db, "Towel Pullup",0,1,1,0,0);
            ExerTable.addInitialExercise(db, "Pullups",0,1,1,0,0);
            ExerTable.addInitialExercise(db, "Knees to Elbows",0,0,0,0,0);
            ExerTable.addInitialExercise(db, "Jumping Pullups",0,1,1,0,0);
            ExerTable.addInitialExercise(db, "Ball Slams",0,1,1,0,0);
            ExerTable.addInitialExercise(db, "Hip Extension",0,0,0,0,0);
            ExerTable.addInitialExercise(db, "Front Lever Pull-ups",0,1,1,0,0);
            ExerTable.addInitialExercise(db, "Kipping Pushup",0,1,1,0,0);
            ExerTable.addInitialExercise(db, "Rope Climb",0,1,1,0,0);
            ExerTable.addInitialExercise(db, "Wall Ball",0,1,1,0,0);
            ExerTable.addInitialExercise(db, "100m Sprint",0,0,0,1,1);
            ExerTable.addInitialExercise(db, "200m Sprint",0,0,0,1,1);
            ExerTable.addInitialExercise(db, "50m Sprint",0,0,0,1,1);
            ExerTable.addInitialExercise(db, "400m Run",0,0,0,1,1);
            ExerTable.addInitialExercise(db, "800m Run",0,0,0,1,1);
            ExerTable.addInitialExercise(db, "Mile Run",0,0,0,1,1);
		
	}
}
