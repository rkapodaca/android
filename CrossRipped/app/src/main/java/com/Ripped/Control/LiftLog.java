package com.Ripped.Control;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.Ripped.Model.LiftTrackerTable;

public class LiftLog {
	
	public static ArrayAdapter<CharSequence> displayList(Context ctx, Spinner spinner, int arrayId)
	{
		
	    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
	            ctx, arrayId, android.R.layout.simple_spinner_item);
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    
	    
	    return adapter;
	}
	
	
	public static void logWorkout(String name, int weight, Context ctx)
	{
		Date date = new Date(new Date().getTime());
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
		String curDate = dateFormat.format(date);
		
	
		String workoutType;
		if(name.equalsIgnoreCase("Clean and Jerk") || name.equalsIgnoreCase("Snatch"))
			workoutType = "Olympic";
		else
			workoutType = "Power";
		
		LiftTrackerTable db = new LiftTrackerTable(ctx);
		
		if(db.betterWorkOutExists(curDate, name, weight))
			return;
		
		
		db.addLiftTracker(name, workoutType, curDate, weight);
		
	}
	
	
}
