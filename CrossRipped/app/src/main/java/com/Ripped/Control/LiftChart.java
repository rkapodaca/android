package com.Ripped.Control;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.chart.TimeChart;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

import com.Ripped.Model.Lift;
import com.Ripped.Model.LiftTrackerTable;

public class LiftChart {
	
   /* Name: powerLifts()
	* Description: Method to graph power Lifts.
	* Return: The intent for the graphical activity.
	*/
	public Intent powerLifts(Context context) {
		
		// Power Lift 1-Rep Max [Given by database]

		/*int[] deadLiftValues = {145, 165, 185, 190, 0, 210, 230, 240, 255, 265, 285};
		int[] squatValues = {115, 125, 135, 140, 145, 160, 185, 200, 210, 225, 235};
		int[] benchPressValues = {95, 110, 112, 115, 125, 135, 140, 150, 155, 160, 165};
		int[] powerCleanValues = {65, 85, 100, 115, 125, 135, 155, 165, 175, 185, 215};
	    */
		
		 // Olympic Lift 1-Rep Max in terms of Pounds (Lbs.)
		LiftTrackerTable db = new LiftTrackerTable(context);
		
		ArrayList<Lift> deadLiftList = db.getAllDead();
		ArrayList<Lift> squatList = db.getAllSquats();
		ArrayList<Lift> benchPressList = db.getAllBench();
		ArrayList<Lift> powerCleanList = db.getAllPowerClean();
		
		// Checks if there are values available to chart
		if(deadLiftList.size() == 0 && squatList.size() == 0
				&& benchPressList.size() == 0 && powerCleanList.size() == 0)
			return null;
	
		String first = db.getFirstDate();
		String last = db.getLastDate();
		Date firstDate;
		Date lastDate;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
		
		// Initialize the days for all arrays
		try{
			 firstDate = dateFormat.parse(first);
			 lastDate = dateFormat.parse(last);
		}
		catch(Exception e)
		{
			Log.d("LiftChart", "The dates are incorrectly formated");
			return null;
		}
		
		
		long daysElapsedMillis = lastDate.getTime() - firstDate.getTime();
		Date daysElapsed = new Date(daysElapsedMillis);
		
		int days = (int) (daysElapsed.getTime() / TimeChart.DAY);
		
		int[] deadLiftValues = new int[days+1];
		int[] squatValues = new int[days+1];
		int[] benchPressValues = new int[days+1];
		int[] powerCleanValues = new int[days+1];
		
		// Loop to check Deadlift values are correct
		int j = 0;
		for(int i = 0; i < days+1; i++)
		{
			try{
			      DateFormat dateToString = new SimpleDateFormat("MM/dd/yy");
				
			      long iteratedDate  = firstDate.getTime() + TimeChart.DAY * i;
			      long arrayDate  = deadLiftList.get(j).getTime();
				
			      Date date1 = new Date(iteratedDate);
			      Date date2 = new Date(arrayDate);
				
			      String trueDate = dateToString.format(date1);
			      String workoutDate = dateToString.format(date2);
				
				
			      if(!trueDate.equalsIgnoreCase(workoutDate) && deadLiftValues[i] == 0)
			      {
					arrayDate  = deadLiftList.get(j).getTime();
					j--;
			      }
					
			      if(trueDate.equalsIgnoreCase(workoutDate))
					deadLiftValues[i] = deadLiftList.get(j).getWieght();
				
			      else {
					deadLiftValues[i] = 0;
				  }
			   }
			
			catch(Exception e)
			{
				deadLiftValues[i] = 0;
			}
				j++;
		}
		
		// Loop to check Squat values are correct
		int k = 0;
		for(int i = 0; i < days+1; i++)
		{
			try{
				
			      DateFormat dateToString = new SimpleDateFormat("MM/dd/yy");
				
			      long iteratedDate  = firstDate.getTime() + TimeChart.DAY * i;
			      long arrayDate  = squatList.get(k).getTime();
				
			      Date date1 = new Date(iteratedDate);
			      Date date2 = new Date(arrayDate);
				
			      String trueDate = dateToString.format(date1);
			      String workoutDate = dateToString.format(date2);
				
				
			      if(!trueDate.equalsIgnoreCase(workoutDate) && squatValues[i] == 0)
			      {
					arrayDate  = squatList.get(k).getTime();
					k--;
			      }
					
			      if(trueDate.equalsIgnoreCase(workoutDate))
					squatValues[i] = squatList.get(k).getWieght();
				
			      else {
					squatValues[i] = 0;
				  }
			   }
			
			catch(Exception e)
			{
				squatValues[i] = 0;
			}
				k++;
		}
		
		// Loop to check Bench Press values are correct
		int a = 0;
		for(int i = 0; i < days+1; i++)
		{
			try{
				
			      DateFormat dateToString = new SimpleDateFormat("MM/dd/yy");
				
			      long iteratedDate  = firstDate.getTime() + TimeChart.DAY * i;
			      long arrayDate  = benchPressList.get(a).getTime();
				
			      Date date1 = new Date(iteratedDate);
			      Date date2 = new Date(arrayDate);
				
			      String trueDate = dateToString.format(date1);
			      String workoutDate = dateToString.format(date2);
				
				
			      if(!trueDate.equalsIgnoreCase(workoutDate) && benchPressValues[i] == 0)
			      {
					arrayDate  = benchPressList.get(a).getTime();
					a--;
			      }
					
			      if(trueDate.equalsIgnoreCase(workoutDate))
					benchPressValues[i] = benchPressList.get(a).getWieght();
				
			      else {
					benchPressValues[i] = 0;
				  }
			   }
			
			catch(Exception e)
			{
				benchPressValues[i] = 0;
			}
				a++;
		}
		
		// Loop to check Power Clean values are correct
		int b = 0;
		for(int i = 0; i < days+1; i++)
		{
			try{
				
			      DateFormat dateToString = new SimpleDateFormat("MM/dd/yy");
				
			      long iteratedDate  = firstDate.getTime() + TimeChart.DAY * i;
			      long arrayDate  = powerCleanList.get(b).getTime();
				
			      Date date1 = new Date(iteratedDate);
			      Date date2 = new Date(arrayDate);
				
			      String trueDate = dateToString.format(date1);
			      String workoutDate = dateToString.format(date2);
				
				
			      if(!trueDate.equalsIgnoreCase(workoutDate) && powerCleanValues[i] == 0)
			      {
					arrayDate  = powerCleanList.get(b).getTime();
					b--;
			      }
					
			      if(trueDate.equalsIgnoreCase(workoutDate))
					powerCleanValues[i] = powerCleanList.get(b).getWieght();
				
			      else {
					powerCleanValues[i] = 0;
				  }
			   }
			
			catch(Exception e)
			{
				powerCleanValues[i] = 0;
			}
				b++;
		}
		
		// Length of all arrays
		int[] lengthOfArrays = {deadLiftValues.length, squatValues.length, 
				benchPressValues.length, powerCleanValues.length};
		
		
		// Checks which series has the most values so we get the right amount of X-axis spaces
		int xPositions = 0;
		for (int i = 0; i < lengthOfArrays.length; i++)
		{
			if (lengthOfArrays[i] > xPositions) {
				xPositions = lengthOfArrays[i];
	        }
		}
				
		// Positional spacing of Dates so it's dynamic to input values.
		int[] xValues = new int[xPositions];
		for (int p = 0; p < xPositions; p++)
		{
		   xValues[p] = 2 * p;
		}
				
		// Initializes a series of workouts
		String[] allExercises = {"Deadlift", "Squat", "Bench Press", "Power Clean"};
		TimeSeries[] allSeries = new TimeSeries[allExercises.length];
		
		// Assignment of Series to respective array positions
		for (int l = 0; l < allSeries.length; l++)
		{
		   allSeries[l] = new TimeSeries(allExercises[l]);
		}
				
		// Adds positions and values to series
		for (int n = 0; n < deadLiftValues.length; n++) {
			try{
	    		while (deadLiftValues[n] == 0)
					++n;
	    	}
			catch (Exception e)
			{
				break; 
			}
	        allSeries[0].add(xValues[n], deadLiftValues[n]);
		}
			
	    for (int o = 0; o < squatValues.length; o++) {
	    	try{
	    		while (squatValues[o] == 0)
					++o;
	    	}
			catch (Exception e)
			{
				break; 
			}
		    allSeries[1].add(xValues[o], squatValues[o]);
	    }
			
		for (int p = 0; p < benchPressValues.length; p++) {
			try{
	    		while (benchPressValues[p] == 0)
					++p;
	    	}
			catch (Exception e)
			{
				break; 
			}
		   allSeries[2].add(xValues[p], benchPressValues[p]);
		}
			
		for (int q = 0; q < powerCleanValues.length; q++) {
			try{
	    		while (powerCleanValues[q] == 0)
					++q;
	    	}
			catch (Exception e)
			{
				break; 
			}
		   allSeries[3].add(xValues[q], powerCleanValues[q]);
	    }
				
		// Adds all series to a dataset
	    XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();	
		for (int r = 0; r < allSeries.length; r++) {
		dataset.addSeries(allSeries[r]);
		}
				
		// Render and Color each series and combine all into trueRenderer
		int[] colors = {Color.MAGENTA, Color.BLUE, Color.RED, Color.GREEN};
		XYSeriesRenderer[] smallRenderers = new XYSeriesRenderer[allSeries.length];
		XYMultipleSeriesRenderer trueRenderer = new XYMultipleSeriesRenderer();
		for (int s = 0; s < smallRenderers.length; s++) 
		{
			smallRenderers[s] = new XYSeriesRenderer();
			smallRenderers[s].setColor(colors[s]);
			smallRenderers[s].setPointStyle(PointStyle.CIRCLE);
		    smallRenderers[s].setFillPoints(true);
		    smallRenderers[s].setDisplayChartValues(true);
		    smallRenderers[s].setChartValuesTextSize((float) 15);
		    smallRenderers[s].setLineWidth((float) 3.4);
			smallRenderers[s].setChartValuesSpacing((float) 7.5); 
			smallRenderers[s].setChartValuesTextAlign(Paint.Align.LEFT);
		    trueRenderer.addSeriesRenderer(smallRenderers[s]);
	    }
		
		// Margins: Top, Left, Bottom, Right | Legend | Grid
		trueRenderer.setMargins(new int[] { 0, 40, 15, 0 });
		trueRenderer.setShowLegend(true);
		trueRenderer.setLegendHeight(0);
		trueRenderer.setLegendTextSize(14);
		trueRenderer.setShowGrid(true);
		
		// Title Size
		trueRenderer.setChartTitle("Power Lifts: 1-Rep Max");
		trueRenderer.setChartTitleTextSize((float) 18);
		trueRenderer.setLabelsTextSize((float)12);
		
		// Point Size
		trueRenderer.setPointSize((float) 5);
				
		// Anti-Aliasing
		trueRenderer.setAntialiasing(true);
				
		// Zoom Capability
		trueRenderer.setZoomButtonsVisible(true);
				
		// Panning Limitations
		trueRenderer.setPanEnabled(true, false);
		double[] panLimits = {-0.5, xValues[xValues.length-1] + 2, 0.0, 0.0};
		trueRenderer.setPanLimits(panLimits);

		// X-Axis Customizations
		trueRenderer.setXAxisMin(0.0);
		trueRenderer.setXLabels(0);
	    trueRenderer.setXAxisMax(7);
		trueRenderer.setXTitle("Date");
				
		// X-Axis: Labeling Dates on positions (Starts on day of first entry)
		long dateinit = new Date().getTime() - xValues.length * TimeChart.DAY;
		Date date = new Date(dateinit);
		
		
		for (int t = 0; t < xValues.length; t++)
		{
			String trueDate = dateFormat.format(date);    
			trueRenderer.addXTextLabel(xValues[t], trueDate);
		    trueRenderer.addXTextLabel(xValues[t] + 1.0, "");
		    Date temp = date;
		    long datemillis = date.getTime();
			long nextdatemillis = datemillis  + TimeChart.DAY;
			temp = new Date(nextdatemillis);
			date = temp;
		}
				
		// Y-Axis Customizations
		ArrayList<Integer> largestNumbers = new ArrayList<Integer>();
		
		int large0 = 0;
		for (int u = 0; u < deadLiftValues.length; u++) 
		{
			 if (deadLiftValues[u] > large0) 
			 {
				  large0 = deadLiftValues[u];
				  largestNumbers.add(large0);
			 }
		}
		
		int large1 = 0;
		for (int v = 0; v < squatValues.length; v++) 
		{
			 if (squatValues[v] > large1) 
			 {
				  large1 = squatValues[v];
				  largestNumbers.add(large1);
			 }
		}
		
		int large2 = 0;
		for (int w = 0; w < benchPressValues.length; w++) 
		{
			 if (benchPressValues[w] > large2) 
			 {
				  large2 = benchPressValues[w];
				  largestNumbers.add(large2);
			 }
		}
		
		int large3 = 0;
		for (int x = 0; x < powerCleanValues.length; x++) 
		{
			 if (powerCleanValues[x] > large3) 
			 {
				  large3 = powerCleanValues[x];
				  largestNumbers.add(large3);
			 }
		}
		
		int largest = 0;
		for (int y = 0; y < largestNumbers.size(); y++)
		{
			if (largestNumbers.get(y) > largest)
			{
			   largest = largestNumbers.get(y);
			}
		}
		
	    trueRenderer.setYAxisMin(0.0);
		trueRenderer.setYAxisMax(largest+30);
		trueRenderer.setYTitle("1-Rep Max (Lbs.)");
		trueRenderer.setYLabels(15);
		trueRenderer.setYLabelsAlign(Paint.Align.RIGHT);
	    trueRenderer.setYAxisAlign(Paint.Align.LEFT, 0);

		// Returns intent of this method
	    return ChartFactory.getLineChartIntent(context, dataset, trueRenderer);
	}
			 
   /* Name: olympicLifts()
	* Description: Method to graph Olympic Lifts.
	* Return: The intent for the graphical activity.
	*/
	public Intent olympicLifts(Context context) {
			
		 // Olympic Lift 1-Rep Max in terms of Pounds (Lbs.)
		LiftTrackerTable db = new LiftTrackerTable(context);
		
		ArrayList<Lift> cleanJerkList = db.getAllCleanJerks();
		ArrayList<Lift> snatchList = db.getAllSnatch();
		
		if(cleanJerkList.size() == 0 && snatchList.size() == 0)
			return null;
	
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
		
		String first = db.getFirstDate();
		Log.d(""+first, "FIRST DATE WTF");
		String last = db.getLastDate();
		Log.d(""+last, "LAST DATE WTF");
		Date firstDate;
		Date lastDate;
		
		try{
			 firstDate = dateFormat.parse(first);
			 lastDate = dateFormat.parse(last);
		}
		catch(Exception e)
		{
			Log.d("LiftChart", "The dates are incorrectly formated");
			return null;
		}
		
		
		long daysElapsedMillis = lastDate.getTime() - firstDate.getTime();
		Date daysElapsed = new Date(daysElapsedMillis);
		
		int days = (int) (daysElapsed.getTime()/TimeChart.DAY);
		
		int[] cleanAndJerkValues = new int[days+1];
		
		int[] snatchValues = new int[days+1];
		
		int j = 0;
		for(int i = 0; i < days+1; i++)
		{
			try{
				Log.d("chart", " clean "+cleanJerkList.get(i).getWieght());
				Log.d("chart", "clean time " + firstDate.getTime() + TimeChart.DAY * i +" "+ cleanJerkList.get(i).getTime());
				
				DateFormat dateToString = new SimpleDateFormat("MM/dd/yy");
				
				long iteratedDate  = firstDate.getTime() + TimeChart.DAY * i;
				long arrayDate  = cleanJerkList.get(j).getTime();
				
				Date date1 = new Date(iteratedDate);
				Date date2 = new Date(arrayDate);
				
				String trueDate = dateToString.format(date1);
				String workoutDate = dateToString.format(date2);
				
				
				if(!trueDate.equalsIgnoreCase(workoutDate) && cleanAndJerkValues[i] == 0)
				{
					arrayDate  = cleanJerkList.get(j).getTime();
					j--;
				}
				Log.d("date", trueDate + " " + workoutDate +" clean an THESE STRINGS SHOULD BE THE SAME");
					
				if(trueDate.equalsIgnoreCase(workoutDate))
					cleanAndJerkValues[i] = cleanJerkList.get(j).getWieght();
				
				else {
					cleanAndJerkValues[i] = 0;
				
				}
				
			}
			catch(Exception e)
			{
				Log.d("chart", i+ " clean " + e);
				Log.d("chart", i +" clean going in as zero");
				cleanAndJerkValues[i] = 0;
			}
			j++;
		}
		
		// Snatch Values
		j = 0;
		for(int i = 0; i < days+1; i++)
		{
			try{
				Log.d("chart", "snatch "+snatchList.get(i).getWieght());
				Log.d("chart", "snatch days " + firstDate.getTime() + TimeChart.DAY * i +" " + snatchList.get(i).getTime());
				
				DateFormat dateToString = new SimpleDateFormat("MM/dd/yy");
				
				long iteratedDate  = firstDate.getTime() + TimeChart.DAY * i;
				long arrayDate  = snatchList.get(j).getTime();
				
				Date date1 = new Date(iteratedDate);
				Date date2 = new Date(arrayDate);
				
				String trueDate = dateToString.format(date1);
				String workoutDate = dateToString.format(date2);
				
				if(!trueDate.equalsIgnoreCase(workoutDate) && snatchValues[i] == 0)
				{
					arrayDate  = snatchList.get(j).getTime();
					j--;
				}
				Log.d("date", trueDate + " " + workoutDate +" snatch an THESE STRINGS SHOULD BE THE SAME");
				
				if(firstDate.getTime() + TimeChart.DAY * i == snatchList.get(j).getTime())
					snatchValues[i] = snatchList.get(j).getWieght();
				else
					snatchValues[i] = 0;
			}
			catch(Exception e)
			{
				Log.d("chart", i+ " snatch " + e);
				Log.d("chart", i +" snatch going in as zero");
				snatchValues[i] = 0;
				
			}
			j++;
		}
		for(int i = 0; i< days+1; i++){
			Log.d("chart", "clean after initializing "+cleanAndJerkValues[i]);
			Log.d("chart", "snatch after initializing "+snatchValues[i]);
		}
		
		
		
		// Olympic Lift 1-Rep Max in terms of Pounds (Lbs.)
	//	int[] cleanAndJerkValues = {20, 56, 80, 132, 150, 165, 178, 200, 
	//	   210, 215, 235};
	//	int[] snatchValues = {5, 14, 20, 34, 55, 59, 60, 70, 80, 133, 150};
		
		// Length of all arrays
		int[] lengthOfArrays = {cleanAndJerkValues.length, snatchValues.length};
		
		// Checks which series has the most values so we get 
		// the right amount of X-axis spaces
		int xPositions = 0;
		for (int i = 0; i < lengthOfArrays.length; i++)
		{
			if (lengthOfArrays[i] > xPositions) {
				xPositions = lengthOfArrays[i];
	        }
		}
				
		// Positional spacing of Dates so it's dynamic to input values.
		int[] xValues = new int[xPositions];
		for (int k = 0; k < xPositions; k++)
		{
		   xValues[k] = 2 * k;
		}
				
		// Initializes a series of workouts
		String[] allExercises = {"Clean And Jerk", "Snatch"};
		TimeSeries[] allSeries = new TimeSeries[allExercises.length];
		
		// Assignment of Series to respective array positions
		for (int l = 0; l < allSeries.length; l++)
		{
		   allSeries[l] = new TimeSeries(allExercises[l]);
		}
				
		// Adds positions and values to series
		
		for (int n = 0; n < cleanAndJerkValues.length; n++) {
			Log.d("chart", "cleans adding to chart: " +cleanAndJerkValues[n]);	
			try{
			while(cleanAndJerkValues[n] == 0)
				n++;
			}
			catch (Exception e)
			{
				break; 
			}
			
		
	    allSeries[0].add(xValues[n], cleanAndJerkValues[n]);
		}
			
	    for (int o = 0; o < snatchValues.length; o++) {
	    	try{
	    		while (snatchValues[o] == 0)
					++o;
	    	}
			catch (Exception e)
			{
				break; 
			}
	    
		allSeries[1].add(xValues[o], snatchValues[o]);
	    }
				
		// Adds all series to a dataset
	    XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();	
		for (int r = 0; r < allSeries.length; r++) {
		dataset.addSeries(allSeries[r]);
		}
				
		// Render and Color each series and combine all into trueRenderer
		int[] colors = {Color.BLUE, Color.GREEN, Color.RED, Color.MAGENTA};
		XYSeriesRenderer[] smallRenderers = new XYSeriesRenderer[allSeries.length];
		XYMultipleSeriesRenderer trueRenderer = new XYMultipleSeriesRenderer();
		for (int s = 0; s < smallRenderers.length; s++) 
		{
			smallRenderers[s] = new XYSeriesRenderer();
			smallRenderers[s].setColor(colors[s]);
			smallRenderers[s].setPointStyle(PointStyle.CIRCLE);
		    smallRenderers[s].setFillPoints(true);
		    smallRenderers[s].setDisplayChartValues(true);
		    smallRenderers[s].setChartValuesTextSize((float) 15);
		    smallRenderers[s].setLineWidth((float) 3.4);
			smallRenderers[s].setChartValuesSpacing((float) 7.5); 
			smallRenderers[s].setChartValuesTextAlign(Paint.Align.RIGHT);
		    
		    trueRenderer.addSeriesRenderer(smallRenderers[s]);
	    }
		
		// Margins: Top, Left, Bottom, Right | Legend | Grid
		trueRenderer.setMargins(new int[] { 0, 40, 15, 0 });
		trueRenderer.setShowLegend(true);
		trueRenderer.setLegendHeight(0);
		trueRenderer.setLegendTextSize(14);
		trueRenderer.setShowGrid(true);

		// Title Size
		trueRenderer.setChartTitle("Olympic Lifts: 1-Rep Max");
		trueRenderer.setChartTitleTextSize((float) 18);
		trueRenderer.setLabelsTextSize((float)12);
				
		// Anti-Aliasing
		trueRenderer.setAntialiasing(true);
		
		// Point Size
		trueRenderer.setPointSize((float) 5);
				
		// Zoom Capability
		trueRenderer.setZoomButtonsVisible(true);
				
		// Panning Limitations
		trueRenderer.setPanEnabled(true, false);
		//double[] panLimits = {-0.5, xValues[xValues.length-1] + 2, 0.0, 0.0};
		//trueRenderer.setPanLimits(panLimits);

		// X-Axis Customizations
		trueRenderer.setXAxisMin(0.0);
		trueRenderer.setXLabels(0);
		//trueRenderer.setXAxisMax(xValues[xValues.length-1]);
	    trueRenderer.setXAxisMax(7);
		trueRenderer.setXTitle("Date");
				
		// X-Axis: Labeling Dates on positions (Starts on day of first entry)
		long dateinit = new Date().getTime() - xValues.length * TimeChart.DAY;
		Date date = new Date(dateinit);
		
		
		for (int t = 0; t < xValues.length; t++)
		{
			String trueDate = dateFormat.format(date);    
			trueRenderer.addXTextLabel(xValues[t], trueDate);
		    trueRenderer.addXTextLabel(xValues[t] + 1.0, "");
		    Date temp = date;
		    long datemillis = date.getTime();
			long nextdatemillis = datemillis  + TimeChart.DAY;
			temp = new Date(nextdatemillis);
			date = temp;
		}
				
		// Y-Axis Customizations
		ArrayList<Integer> largestNumbers = new ArrayList<Integer>();
		
		int large0 = 0;
		for (int u = 0; u < cleanAndJerkValues.length; u++) 
		{
			 if (cleanAndJerkValues[u] > large0) 
			 {
				  large0 = cleanAndJerkValues[u];
				  largestNumbers.add(large0);
			 }
		}
		
		int large1 = 0;
		for (int v = 0; v < snatchValues.length; v++) 
		{
			 if (snatchValues[v] > large1) 
			 {
				  large1 = snatchValues[v];
				  largestNumbers.add(large1);
			 }
		}
		
		int largest = 0;
		for (int y = 0; y < largestNumbers.size(); y++)
		{
			if (largestNumbers.get(y) > largest)
			{
			   largest = largestNumbers.get(y);
			}
		}
		
	    trueRenderer.setYAxisMin(0.0);
		trueRenderer.setYAxisMax(largest+10);
		trueRenderer.setYTitle("1-Rep Max (Lbs.)");
		trueRenderer.setYLabels(15);
		trueRenderer.setYLabelsAlign(Paint.Align.RIGHT);
	    trueRenderer.setYAxisAlign(Paint.Align.LEFT, 0);

		// Returns intent of this method
	    return ChartFactory.getLineChartIntent(context, dataset, trueRenderer);
	}	
}