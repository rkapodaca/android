package com.Ripped.CrossRipped;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.Ripped.Control.LiftLog;
import com.Ripped.Control.Menu;

public class OlympicLogMenu extends Menu   {
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logbook);
        
        Spinner spinner = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = LiftLog.displayList(this, spinner, R.array.olympiclifts_array);
        
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
       
	}

}
	
	
	
