package org.kp.sdcatapp2Droid.db;

public class Feedback {
	private int mRate;
	private String mDetails = "";
	
	public Feedback(int rate, String det){
		mRate = rate;
		mDetails = det;
	}
	
	public void setFeedback(int rate, String det){
		mRate = rate;
		mDetails = det;
	}
	
	public int getRate(){
		return mRate;
	}
	
	public String getDetails(){
		return mDetails;
	}
}
