
package com.Ripped.Control;

import com.Ripped.CrossRipped.R;
import com.Ripped.CrossRipped.R.id;

import android.content.Context;
import android.os.Handler;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NumberPicker extends LinearLayout {

	private final long REPEAT_DELAY = 50;
	
	private final int ELEMENT_HEIGHT = 110;
	private final int ELEMENT_WIDTH = 80; 
	
	
	protected int MINIMUM = 0;
	protected int MAXIMUM = 11;
	
	
	
	private final int TEXT_SIZE = 25;
	private final int TEXT_SIZEbutton = 30;
	
	
	public Integer value = 0;
	
	Button decrement;
	Button increment;
	public EditText valueText;
	public EditText oHeight;
	
	
	private Handler repeatUpdateHandler = new Handler();
	
	private boolean autoIncrement = false;
	private boolean autoDecrement = false;

	
	class RepetetiveUpdater implements Runnable {
		public void run() {
			if( autoIncrement ){
				increment();
				repeatUpdateHandler.postDelayed( new RepetetiveUpdater(), REPEAT_DELAY );
			} else if( autoDecrement ){
				decrement();
				repeatUpdateHandler.postDelayed( new RepetetiveUpdater(), REPEAT_DELAY );
			}
		}
	}
	
	public NumberPicker( Context context, AttributeSet attributeSet ) {
		super(context, attributeSet);
		
		 
	
		this.setLayoutParams( new LinearLayout.LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT ) );
		LayoutParams elementParams = new LinearLayout.LayoutParams( ELEMENT_HEIGHT, ELEMENT_WIDTH );
		
	
		initDecrementButton( context );
		initValueEditText( context );
		initIncrementButton( context );
			
		if( getOrientation() == VERTICAL ){
			addView( increment, elementParams );
			addView( valueText, elementParams );
			addView( decrement, elementParams );
		} else {
			addView( decrement, elementParams );
			addView( valueText, elementParams );
			addView( increment, elementParams );
		}
	}
	
	private void initIncrementButton( Context context){
		increment = new Button( context );
		increment.setTextSize( TEXT_SIZEbutton );
		increment.setText( "+" );
		increment.setBackgroundResource(00000000);
		increment.setTextColor(-1);
		
		
	
		
		// Increment once for a click
		increment.setOnClickListener(new View.OnClickListener() {
			
            public void onClick(View v) {
                 	
            	increment();
            	
            }
        });
		
		// Auto increment for a long click
		increment.setOnLongClickListener( 
				new View.OnLongClickListener(){
					
					public boolean onLongClick(View arg0) {
						autoIncrement = true;
						
						repeatUpdateHandler.post( new RepetetiveUpdater() );
						return false;
					}
				}
		);
		
		// When the button is released, if we're auto incrementing, stop
		increment.setOnTouchListener( new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				
				if( event.getAction() == MotionEvent.ACTION_UP  ){
					if (autoIncrement){
						autoIncrement = false;
					}
					increment.setTextColor(-1);
				}
				
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					increment.setTextColor(-65536);
				}
				return false;
			}
		});
		
	}
	
	private void initValueEditText( Context context){
		
		//value = 5;
		
		valueText = new EditText( context );
		valueText.setTextSize( TEXT_SIZE );
		valueText.setBackgroundResource(00000000);
		valueText.setTextColor(-1);
		oHeight = (EditText)findViewById(R.id.outHeight); 
		
		
		// Since we're a number that gets affected by the button, we need to be
		// ready to change the numeric value with a simple ++/--, so whenever
		// the value is changed with a keyboard, convert that text value to a
		// number. We can set the text area to only allow numeric input, but 
		// even so, a carriage return can get hacked through. To prevent this
		// little quirk from causing a crash, store the value of the internal
		// number before attempting to parse the changed value in the text area
		// so we can revert to that in case the text change causes an invalid
		// number
		valueText.setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int arg1, KeyEvent event) {
				int backupValue = value;
				try {
					value = Integer.parseInt( ((EditText)v).getText().toString() );
				} catch( NumberFormatException nfe ){
					value = backupValue;
				}
				return false;
			}
		});
		
		// Highlight the number when we get focus
		valueText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				if( hasFocus ){
					((EditText)v).selectAll();
				}
			}
		});
		valueText.setGravity( Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL );
		valueText.setText( value.toString() );
		valueText.setInputType( InputType.TYPE_CLASS_NUMBER );
	}
	
	private void initDecrementButton( Context context){
		decrement = new Button( context );
		decrement.setTextSize( TEXT_SIZEbutton );
		decrement.setText( "-" );
		decrement.setBackgroundResource(00000000);
		decrement.setTextColor(-1);
		

		// Decrement once for a click
		decrement.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	decrement();
            }
        });
		

		// Auto Decrement for a long click
		decrement.setOnLongClickListener( 
				new View.OnLongClickListener(){
					public boolean onLongClick(View arg0) {
						autoDecrement = true;
						repeatUpdateHandler.post( new RepetetiveUpdater() );
						return false;
					}
				}
		);
		
		// When the button is released, if we're auto decrementing, stop
		decrement.setOnTouchListener( new View.OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
				
				if( event.getAction() == MotionEvent.ACTION_UP  ){
					if (autoDecrement){
					autoDecrement = false;
					}
					decrement.setTextColor(-1);
					
					
				}
				
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					decrement.setTextColor(-65536);
				}
				return false;
			}
		});
	}
	
	public void increment(){
		if( value < MAXIMUM ){
			value = value + 1;
			valueText.setText( value.toString() );
		}
	}

	public void decrement(){
		if( value > MINIMUM ){
			value = value - 1;
			valueText.setText( value.toString() );
		}
	}
	
	public int getValue(){
		return value;
	}
	
	public void setValue( int value ){
		if( value > MAXIMUM ) value = MAXIMUM;
		if( value >= 0 ){
			this.value = value;
			valueText.setText( this.value.toString() );
		}
	}
	
	
}
