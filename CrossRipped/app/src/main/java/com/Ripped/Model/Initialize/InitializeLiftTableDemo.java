package com.Ripped.Model.Initialize;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.database.sqlite.SQLiteDatabase;

import com.Ripped.Model.LiftTrackerTable;

public class InitializeLiftTableDemo {
	public static void initialize(SQLiteDatabase db){
			
			DateFormat dateToString = new SimpleDateFormat("MM/dd/yy");
			long dateMilliseconds = new Date().getTime();
			Date date = new Date(dateMilliseconds);
			
			String todaysDate = dateToString.format(date);
			
			// Olympic Lifts
			LiftTrackerTable.addInitialLiftTracker(db, "Clean and Jerk", "Olympic", "05/31/12", 85);
			LiftTrackerTable.addInitialLiftTracker(db, "Clean and Jerk", "Olympic", "06/01/12", 110);
			LiftTrackerTable.addInitialLiftTracker(db, "Clean and Jerk", "Olympic", "06/02/12", 135);
			LiftTrackerTable.addInitialLiftTracker(db, "Clean and Jerk", "Olympic", "06/03/12", 145);
			LiftTrackerTable.addInitialLiftTracker(db, "Clean and Jerk", "Olympic", "06/04/12", 170);
			LiftTrackerTable.addInitialLiftTracker(db, "Clean and Jerk", "Olympic", "06/05/12", 180);
			
	
			LiftTrackerTable.addInitialLiftTracker(db, "Snatch", "Olympic", "05/31/12", 20);
			LiftTrackerTable.addInitialLiftTracker(db, "Snatch", "Olympic", "06/01/12", 35);
			LiftTrackerTable.addInitialLiftTracker(db, "Snatch", "Olympic", "06/02/12", 70);
			LiftTrackerTable.addInitialLiftTracker(db, "Snatch", "Olympic", "06/03/12", 80);
			LiftTrackerTable.addInitialLiftTracker(db, "Snatch", "Olympic", "06/04/12", 90);
			LiftTrackerTable.addInitialLiftTracker(db, "Snatch", "Olympic", "06/05/12", 100);
			
			// Power Lifts
			LiftTrackerTable.addInitialLiftTracker(db, "Dead Lift", "Power", "05/31/12", 85);
			LiftTrackerTable.addInitialLiftTracker(db, "Dead Lift", "Power", "06/01/12", 110);
			LiftTrackerTable.addInitialLiftTracker(db, "Dead Lift", "Power", "06/02/12", 135);
			LiftTrackerTable.addInitialLiftTracker(db, "Dead Lift", "Power", "06/03/12", 145);
			LiftTrackerTable.addInitialLiftTracker(db, "Dead Lift", "Power", "06/04/12", 170);
			LiftTrackerTable.addInitialLiftTracker(db, "Dead Lift", "Power", "06/05/12", 180);
			
			LiftTrackerTable.addInitialLiftTracker(db, "Squat", "Power", "05/31/12", 20);
			LiftTrackerTable.addInitialLiftTracker(db, "Squat", "Power", "06/01/12", 35);
			LiftTrackerTable.addInitialLiftTracker(db, "Squat", "Power", "06/02/12", 70);
			LiftTrackerTable.addInitialLiftTracker(db, "Squat", "Power", "06/03/12", 80);
			LiftTrackerTable.addInitialLiftTracker(db, "Squat", "Power", "06/04/12", 90);
			LiftTrackerTable.addInitialLiftTracker(db, "Squat", "Power", "06/05/12", 100);
			
			LiftTrackerTable.addInitialLiftTracker(db, "Bench Press", "Power", "05/31/12", 25);
			LiftTrackerTable.addInitialLiftTracker(db, "Bench Press", "Power", "06/01/12", 45);
			LiftTrackerTable.addInitialLiftTracker(db, "Bench Press", "Power", "06/02/12", 90);
			LiftTrackerTable.addInitialLiftTracker(db, "Bench Press", "Power", "06/03/12", 100);
			LiftTrackerTable.addInitialLiftTracker(db, "Bench Press", "Power", "06/04/12", 130);
			LiftTrackerTable.addInitialLiftTracker(db, "Bench Press", "Power", "06/05/12", 150);
			
			LiftTrackerTable.addInitialLiftTracker(db, "Power Clean", "Power", "05/31/12", 44);
			LiftTrackerTable.addInitialLiftTracker(db, "Power Clean", "Power", "06/01/12", 66);
			LiftTrackerTable.addInitialLiftTracker(db, "Power Clean", "Power", "06/02/12", 88);
			LiftTrackerTable.addInitialLiftTracker(db, "Power Clean", "Power", "06/03/12", 111);
			LiftTrackerTable.addInitialLiftTracker(db, "Power Clean", "Power", "06/04/12", 155);
			LiftTrackerTable.addInitialLiftTracker(db, "Power Clean", "Power", "06/05/12", 200);
		
	}
}

