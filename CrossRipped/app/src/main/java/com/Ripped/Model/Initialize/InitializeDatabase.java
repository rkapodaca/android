package com.Ripped.Model.Initialize;

import android.database.sqlite.SQLiteDatabase;

public class InitializeDatabase {
	
	public static void initialize(SQLiteDatabase db)
	{
		InitializeExerciseTable.initialize(db);
		InitializeWodTable.initialize(db);
		InitializeLiftTable.initialize(db);
	}
	
}