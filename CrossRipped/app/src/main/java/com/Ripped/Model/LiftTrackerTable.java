package com.Ripped.Model;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class LiftTrackerTable extends DBadapter{
	
    
    public LiftTrackerTable(Context context) {
		super(context);
	}
    
	
	// Adding new lift tracker
    public static void addInitialLiftTracker(SQLiteDatabase db, String liftname, String type, String date_of_lift, int max_reps) {

		
		ContentValues values = new ContentValues();
		values.put(KEY_LIFTNAME, liftname);
		values.put(KEY_TYPE,type);
		values.put(KEY_LIFT_DATE, date_of_lift);
		values.put(KEY_REPMAX, max_reps);
		
		// Inserting Row
		db.insert(TABLE_LIFT_TRACKER, null, values);
		
	}
    
    
	public void addLiftTracker(String liftname, String type, String date_of_lift, int max_reps) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		
		
		
		ContentValues values = new ContentValues();
		values.put(KEY_LIFTNAME, liftname);
		values.put(KEY_TYPE,type);
		values.put(KEY_LIFT_DATE, date_of_lift);
		values.put(KEY_REPMAX, max_reps);
		
		// Inserting Row
		db.insert(TABLE_LIFT_TRACKER, null, values);
		db.close(); // Closing database connection
	}
	
	//get rep_max from the date

	public String getFirstDate()
	{
		String selectQuery = "SELECT " + KEY_LIFT_DATE+ " FROM " + TABLE_LIFT_TRACKER +	
			" where " + KEY_ROWID + " == 1" ;
		SQLiteDatabase db = this.open();
		Cursor cursor = db.rawQuery(selectQuery, null);
		String val = " ";
		if (cursor.moveToLast())
			val =cursor.getString(0);
		
		cursor.close();
		db.close();
	
		return val;
	}
	
	public String getLastDate()
	{
			String selectQuery = "SELECT " + KEY_LIFT_DATE+ " FROM " + TABLE_LIFT_TRACKER;
		SQLiteDatabase db = this.open();
		Cursor cursor = db.rawQuery(selectQuery, null);
		String val = " ";
		if (cursor.moveToLast())
			val =cursor.getString(0);
		cursor.close();
		db.close();
	
		return val;
	}
	
	public ArrayList<String> getAllLifts() {
		ArrayList<String> liftTrack = new ArrayList<String>();
		// Select All Query
		String selectQuery = "SELECT "+ KEY_LIFT_DATE +" FROM " + TABLE_LIFT_TRACKER +
						" where " + KEY_LIFT_DATE + " == 1";
		
		SQLiteDatabase db = this.open();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
		do {
			String exer = cursor.getString(1) + " " + cursor.getString(2) 
				+ " "+ cursor.getString(3)+ " "+ cursor.getString(4);
			// Adding contact to list
			liftTrack.add(exer);
		} while (cursor.moveToNext());
	}
	db.close();
	cursor.close();
	// return contact list
	return liftTrack;
	}
	
	public ArrayList<Lift> getAllCleanJerks(){
		ArrayList<Lift> liftTrack = new ArrayList<Lift>();
		String selectQuery = "SELECT * FROM " + TABLE_LIFT_TRACKER +
				" where " + KEY_LIFTNAME + " = 'Clean and Jerk'";
		
		SQLiteDatabase db = this.open();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Lift l;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
		do {
			l = new Lift( cursor.getString(1), cursor.getString(2),
					cursor.getString(3), Integer.parseInt(cursor.getString(4)));
			// Adding contact to list
			liftTrack.add(l);
		} while (cursor.moveToNext());
	}
	db.close();
	cursor.close();
	// return contact list
	return liftTrack;
	}
	
	public ArrayList<Lift> getAllSnatch(){
		ArrayList<Lift> liftTrack = new ArrayList<Lift>();
		String selectQuery = "SELECT * FROM " + TABLE_LIFT_TRACKER +
				" where " + KEY_LIFTNAME + " = 'Snatch'";
		
		SQLiteDatabase db = this.open();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Lift l;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
		do {
			l = new Lift( cursor.getString(1), cursor.getString(2),
					cursor.getString(3), Integer.parseInt(cursor.getString(4)));
			// Adding contact to list
			liftTrack.add(l);
		} while (cursor.moveToNext());
	}
	db.close();
	cursor.close();
	// return contact list
	return liftTrack;
	}
	
	public ArrayList<Lift> getAllDead(){
		ArrayList<Lift> liftTrack = new ArrayList<Lift>();
		String selectQuery = "SELECT * FROM " + TABLE_LIFT_TRACKER +
				" where " + KEY_LIFTNAME + " = 'Dead Lift'";
		
		SQLiteDatabase db = this.open();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Lift l;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
		do {
			l = new Lift( cursor.getString(1), cursor.getString(2),
					cursor.getString(3), Integer.parseInt(cursor.getString(4)));
			// Adding contact to list
			liftTrack.add(l);
		} while (cursor.moveToNext());
	}
	db.close();
	cursor.close();
	// return contact list
	return liftTrack;
	}
	
	public ArrayList<Lift> getAllSquats(){
		ArrayList<Lift> liftTrack = new ArrayList<Lift>();
		String selectQuery = "SELECT * FROM " + TABLE_LIFT_TRACKER +
				" where " + KEY_LIFTNAME + " = 'Squat'";
		
		SQLiteDatabase db = this.open();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Lift l;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
		do {
			l = new Lift( cursor.getString(1), cursor.getString(2),
					cursor.getString(3), Integer.parseInt(cursor.getString(4)));
			// Adding contact to list
			liftTrack.add(l);
		} while (cursor.moveToNext());
	}
	db.close();
	cursor.close();
	// return contact list
	return liftTrack;
	}
	
	public ArrayList<Lift> getAllBench(){
		ArrayList<Lift> liftTrack = new ArrayList<Lift>();
		String selectQuery = "SELECT * FROM " + TABLE_LIFT_TRACKER +
				" where " + KEY_LIFTNAME + " = 'Bench Press'";
		
		SQLiteDatabase db = this.open();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Lift l;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
		do {
			l = new Lift( cursor.getString(1), cursor.getString(2),
					cursor.getString(3), Integer.parseInt(cursor.getString(4)));
			// Adding contact to list
			liftTrack.add(l);
		} while (cursor.moveToNext());
	}
	db.close();
	cursor.close();
	// return contact list
	return liftTrack;
	}
	
	public ArrayList<Lift> getAllPowerClean(){
		ArrayList<Lift> liftTrack = new ArrayList<Lift>();
		String selectQuery = "SELECT * FROM " + TABLE_LIFT_TRACKER +
				" where " + KEY_LIFTNAME + " = 'Power Clean'";
		
		SQLiteDatabase db = this.open();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Lift l;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
		do {
			l = new Lift( cursor.getString(1), cursor.getString(2),
					cursor.getString(3), Integer.parseInt(cursor.getString(4)));
			// Adding contact to list
			liftTrack.add(l);
		} while (cursor.moveToNext());
	}
	db.close();

	cursor.close();
	// return contact list
	return liftTrack;
	}


	
	
	
	public boolean betterWorkOutExists(String curDate, String name, int weight) {
		String selectQuery = "SELECT * FROM " + TABLE_LIFT_TRACKER +
		" where " + KEY_LIFTNAME + " = '" + name+"' AND " + KEY_LIFT_DATE + " = '" 
		+ curDate+ "'"; 

		SQLiteDatabase db = this.open();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				if(weight < Integer.parseInt(cursor.getString(4)))
				{
					Log.d("BWO", cursor.getString(4));
					db.close();
					cursor.close();
					return true;
				}
			} while (cursor.moveToNext());
		}
		
		Log.d("BWO", "returning false");
		cursor.moveToFirst();
		if(cursor.getCount() != 0)
			deleteLift(cursor.getString(1), cursor.getString(2), cursor.getString(3), Integer.parseInt(cursor.getString(4)));
		db.close();
		cursor.close();
		return false;
	}


	private void deleteLift(String name, String type, String curDate,  int weight) {
		String delete =  KEY_LIFTNAME + " = '" + name + "' AND " + KEY_TYPE + " = '" + type + "' AND "
				+ KEY_LIFT_DATE + " = '" + curDate + "' AND " + KEY_REPMAX + " == " + weight ;
		Log.d("BWO", "deleting element");
		SQLiteDatabase db = this.open();
		db.delete(TABLE_LIFT_TRACKER, delete, null);
		db.close();
	}
	
	
	
}
