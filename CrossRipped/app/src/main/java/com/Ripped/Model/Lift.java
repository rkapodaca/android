package com.Ripped.Model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.util.Log;

public class Lift {
	private String name;
	private String type;
	private String sDate;
	private int weight;
	private Date date;
	
	public Lift()
	{
		
	}
	
	public Lift(String n, String t, String d, int w)
	{
		name = n;
		type = t;
		sDate = d;
		weight = w;
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
		Log.d("lift", " can we see this");
		try{
		
			date = dateFormat.parse(d);
			Log.d("lift", "date parsing was successful");
		}
		catch(Exception e)
		{
			Log.d("lift", " Date not parseing " + e );
			
		}
		
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	public void setSDate(String date) {
		this.sDate = date;
	}
	
	public String getSDate() {
		return sDate;
	}
	public long getTime()
	{
		return date.getTime();
	}
	
	public void setWieght(int wieght) {
		this.weight = wieght;
	}
	public int getWieght() {
		return weight;
	}
	
	public String toString()
	{
		return name + " " + type + " " + date + " " + weight;
	}
}
