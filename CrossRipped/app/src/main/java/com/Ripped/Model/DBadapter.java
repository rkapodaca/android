package com.Ripped.Model;

import com.Ripped.Model.Initialize.InitializeDatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBadapter extends SQLiteOpenHelper {
	
	
	//Database Version
	protected static int DATABASE_VERSION = 13;
	// Database name
	protected static final String DATABASE_NAME = "Data_Manager";
	
	//Table names
	protected static final String TABLE_EXERCISES = "Exercises";
	protected static final String TABLE_LIFT_TRACKER = "LiftTracker";
	protected static final String TABLE_WODS = "Wods";
	protected static final String TABLE_CUSTOM = "CustomWods";
	
	
	
	//Primary key for tables
	protected static final String KEY_ROWID = "_id";
	
	//FitTracker Table columns names
	protected static final String KEY_DATE = "date_of_in";
	protected static final String KEY_CURRENTWEIGHT = "current_weight";
	protected static final String KEY_HEIGHT = "height";
	protected static final String GENDER = "gender"; //0 is male, 1 is female
    
    //ExerIncluded Table column names
	protected static final String KEY_EXID = "ex_id";
	protected static final String KEY_WDID = "wd_id";
	protected static final String KEY_RECOMMENDEDWEIGHT = "recommeded_weight";
	protected static final String KEY_RECOMMENDEDREPS= "recommended_reps";
    
	//Custom table column names
	protected static final String KEY_CUST_NAME = "custom_name";
	protected static final String KEY_TIMETOFINISHWOD = "timeToFinishWod";
	protected static final String KEY_WODSTRUCT = "wodstruct";
   
	//Exercise Table column names
	protected static final String KEY_EXER_NAME = "exername";
	protected static final String KEY_DEFAULTMANWEIGHT = "default_man_weight";
	protected static final String KEY_DEFAULTWOMANWEIGHT = "default_woman_weight";
	protected static final String KEY_WEIGHTS_FILT = "weights_filter";
	protected static final String KEY_OLYMPIC_FILT = "olympic_filter";
	protected static final String KEY_EQUIPNEED_FILT = "equipment_need_filter";
	protected static final String KEY_UPPERBODY_FILT = "upperbody_filter";
	protected static final String KEY_LOWERBODY_FILT = "lowerbody_filter";
	protected static final String KEY_LARGEAREA_FILT = "large_area_filter";

    //LiftTracker Table column names
	protected static final String KEY_LIFTNAME = "liftname"; 
	protected static final String KEY_TYPE = "type";
	protected static final String KEY_LIFT_DATE = "liftdate";
	protected static final String KEY_REPMAX = "repsmax";
	
    
    //Wod Table column names
	protected static final String KEY_WOD_NAME = "wodname";
    protected static final String KEY_TYPWOD = "typewod" ; 
    protected static final String KEY_SETSTRUCT = "setstruct";
    protected static final String KEY_REPFORTIME = "repsfortime";
    protected static final String KEY_TIMEFORFINISHREPS = "timeToFinishReps";
    
   
    
    public DBadapter(Context context) {
    	super(context, DATABASE_NAME, null, DATABASE_VERSION); 
	}
	

	
	public void onCreate(SQLiteDatabase db) {	
		String CREATE_EXERCISES_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_EXERCISES + "("
			+ KEY_ROWID + " INTEGER PRIMARY KEY, " 
			+ KEY_EXER_NAME + " TEXT, "
			+ KEY_WEIGHTS_FILT + " INTEGER, "
			+ KEY_EQUIPNEED_FILT + " INTEGER, "
			+ KEY_UPPERBODY_FILT + " INTEGER, "
			+ KEY_LOWERBODY_FILT + " INTEGER, "
			+ KEY_LARGEAREA_FILT + " INTEGER);";
		
		String CREATE_LIFTTRACK_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_LIFT_TRACKER + "("
			+ KEY_ROWID + " INTEGER PRIMARY KEY, "
			+ KEY_LIFTNAME + " TEXT, "
			+ KEY_TYPE + " TEXT, "
			+ KEY_LIFT_DATE + " TEXT, "
			+ KEY_REPMAX + " INTEGER);";
				
		
		String CREATE_WODS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_WODS + "("
			+ KEY_ROWID + " INTEGER PRIMARY KEY, " 
			+ KEY_WOD_NAME + " TEXT, "
			+ KEY_TYPWOD + " TEXT, "
	    	+ KEY_SETSTRUCT + " TEXT, "
	    	+ KEY_REPFORTIME + " INTEGER, "
	    	+ KEY_TIMEFORFINISHREPS + " TEXT);";
	    		
	    		
	   	
	   	String CREATE_GENWOD_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_CUSTOM + "("
	   	+ KEY_ROWID + " INTEGER PRIMARY KEY, " 
	   	+ KEY_CUST_NAME+ " TEXT, "  
	   	+ KEY_TIMETOFINISHWOD + " TEXT, "
	   	+ KEY_WODSTRUCT + " TEXT);";	
	    	    		
	   	db.execSQL(CREATE_EXERCISES_TABLE); 
	   	db.execSQL(CREATE_LIFTTRACK_TABLE);
		db.execSQL(CREATE_WODS_TABLE);
		db.execSQL(CREATE_GENWOD_TABLE);
		
		InitializeDatabase.initialize(db);
	 		
	}

	public SQLiteDatabase open()
	{
		try{
				SQLiteDatabase db = this.getWritableDatabase();
				return db;
		}
	
		
		catch(SQLiteException e){
				Log.e("Db", "Database could not be opened");
				return null;
		}
	}

	public void onUpgrade(SQLiteDatabase db,  int oldVersion, int newVersion) {
		// Drop older tables if existed
		if(oldVersion < newVersion){
	        Log.d("version", "got new version");
			
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXERCISES);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIFT_TRACKER);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_WODS);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOM);
			// Create tables again
			onCreate(db);
			
			
		}
	}
	public void onReset(SQLiteDatabase db) {
		// Drop older tables if existed
	
	        Log.d("reset", "all tables");
			//db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXERCISESINCLUDED);
			//db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXERCISES);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIFT_TRACKER);
			//db.execSQL("DROP TABLE IF EXISTS " + TABLE_WODS);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOM);
			// Create tables again
			onCreate(db);
			
			
;
	}
	



}
