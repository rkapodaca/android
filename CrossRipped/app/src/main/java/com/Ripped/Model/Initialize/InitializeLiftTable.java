package com.Ripped.Model.Initialize;

import com.Ripped.Model.LiftTrackerTable;

import android.database.sqlite.SQLiteDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class InitializeLiftTable {

	
	
	public static void initialize(SQLiteDatabase db){
		
		DateFormat dateToString = new SimpleDateFormat("MM/dd/yy");
		long dateMilliseconds = new Date().getTime();
		Date date = new Date(dateMilliseconds);
		
		String todaysDate = dateToString.format(date);
		
		LiftTrackerTable.addInitialLiftTracker(db, "Clean and Jerk", "Olympic", "06/08/12", 85);
		LiftTrackerTable.addInitialLiftTracker(db, "Clean and Jerk", "Olympic", "06/09/12", 110);
		LiftTrackerTable.addInitialLiftTracker(db, "Clean and Jerk", "Olympic", "06/010/12", 135);
		LiftTrackerTable.addInitialLiftTracker(db, "Clean and Jerk", "Olympic", "06/011/12", 145);
		LiftTrackerTable.addInitialLiftTracker(db, "Clean and Jerk", "Olympic", "06/012/12", 170);
		LiftTrackerTable.addInitialLiftTracker(db, "Clean and Jerk", "Olympic", "06/013/12", 180);


		LiftTrackerTable.addInitialLiftTracker(db, "Snatch", "Olympic", "06/08/12", 20);
		LiftTrackerTable.addInitialLiftTracker(db, "Snatch", "Olympic", "06/09/12", 35);
		LiftTrackerTable.addInitialLiftTracker(db, "Snatch", "Olympic", "06/10/12", 70);
		LiftTrackerTable.addInitialLiftTracker(db, "Snatch", "Olympic", "06/11/12", 80);
		LiftTrackerTable.addInitialLiftTracker(db, "Snatch", "Olympic", "06/12/12", 90);
		LiftTrackerTable.addInitialLiftTracker(db, "Snatch", "Olympic", "06/13/12", 100);

		// Power Lifts
		LiftTrackerTable.addInitialLiftTracker(db, "Dead Lift", "Power", "06/08/12", 85);
		LiftTrackerTable.addInitialLiftTracker(db, "Dead Lift", "Power", "06/09/12", 110);
		LiftTrackerTable.addInitialLiftTracker(db, "Dead Lift", "Power", "06/10/12", 135);
		LiftTrackerTable.addInitialLiftTracker(db, "Dead Lift", "Power", "06/11/12", 145);
		LiftTrackerTable.addInitialLiftTracker(db, "Dead Lift", "Power", "06/12/12", 170);
		LiftTrackerTable.addInitialLiftTracker(db, "Dead Lift", "Power", "06/13/12", 180);

		LiftTrackerTable.addInitialLiftTracker(db, "Squat", "Power", "06/08/12", 20);
		LiftTrackerTable.addInitialLiftTracker(db, "Squat", "Power", "06/09/12", 35);
		LiftTrackerTable.addInitialLiftTracker(db, "Squat", "Power", "06/10/12", 70);
		LiftTrackerTable.addInitialLiftTracker(db, "Squat", "Power", "06/11/12", 80);
		LiftTrackerTable.addInitialLiftTracker(db, "Squat", "Power", "06/12/12", 90);
		LiftTrackerTable.addInitialLiftTracker(db, "Squat", "Power", "06/13/12", 100);

		LiftTrackerTable.addInitialLiftTracker(db, "Bench Press", "Power", "06/08/12", 25);
		LiftTrackerTable.addInitialLiftTracker(db, "Bench Press", "Power", "06/09/12", 45);
		LiftTrackerTable.addInitialLiftTracker(db, "Bench Press", "Power", "06/10/12", 90);
		LiftTrackerTable.addInitialLiftTracker(db, "Bench Press", "Power", "06/11/12", 100);
		LiftTrackerTable.addInitialLiftTracker(db, "Bench Press", "Power", "06/12/12", 130);
		LiftTrackerTable.addInitialLiftTracker(db, "Bench Press", "Power", "06/13/12", 150);

		LiftTrackerTable.addInitialLiftTracker(db, "Power Clean", "Power", "06/08/12", 44);
		LiftTrackerTable.addInitialLiftTracker(db, "Power Clean", "Power", "06/09/12", 66);
		LiftTrackerTable.addInitialLiftTracker(db, "Power Clean", "Power", "06/10/12", 88);
		LiftTrackerTable.addInitialLiftTracker(db, "Power Clean", "Power", "06/11/12", 111);
		LiftTrackerTable.addInitialLiftTracker(db, "Power Clean", "Power", "06/12/12", 155);
		LiftTrackerTable.addInitialLiftTracker(db, "Power Clean", "Power", "06/13/12", 200);
		
		
	}
}
