package com.Ripped.Model;

public class Wod {
	private String name;
	private String type;
	private String instruction;
	private int reps;
	private String time;
	
	
	public Wod()
	{

	}
	
	public Wod(String n, String t, String i, int r, String ti)
	{
		name = n;
		type = t;
		instruction = i;
		reps = r;
		time = ti;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}
	public void setReps(int reps) {
		this.reps = reps;
	}
	public int getReps() {
		return reps;
	}
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	public String getInstruction() {
		return instruction;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getTime() {
		return time;
	}
}
