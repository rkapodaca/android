package com.Ripped.CrossRipped;

import android.os.Bundle;

import com.Ripped.Control.Menu;
import com.Ripped.Model.DBadapter;
import com.Ripped.Model.ExerTable;
import com.Ripped.Model.WodTable;
import com.Ripped.Model.Initialize.InitializeLiftTable;

	
public class MainMenu extends Menu   {
    /* Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
      //  InitializeLiftTableDemo.initialize(db)();
		WodTable wt = new WodTable(this);
		wt.printAllWods();
    }
}

