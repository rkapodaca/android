package com.Ripped.Model.Initialize;

import com.Ripped.Model.WodTable;

import android.database.sqlite.SQLiteDatabase;

public class InitializeWodTable {
			
	public static void initialize(SQLiteDatabase db){
			
			
			WodTable.addInitialWod(db, "Angie","Girls","100 Pull-ups, 100 Push-ups, 100 Sit-ups, 100 Squats",0,"For time");
			WodTable.addInitialWod(db, "Barbara","Girls","20 Pull ups, 30 Push Ups,40 Sit Ups, 50 Squats",0,"5 rounds For time");
			WodTable.addInitialWod(db, "Chelsea","Girls","5 Pull-ups, 10 Push-ups, 15 Squats",0,"Each min on the min for 30 min");		
			WodTable.addInitialWod(db, "Cindy","Girls","5 Pull-ups, 10 Push-ups, 15 Squats",0,"As many rounds as possible in 20 min");
			WodTable.addInitialWod(db, "Diane","Girls","Deadlift 225 lbs, Handstand push-up",0,"21-15-9 reps, For time");
			WodTable.addInitialWod(db, "Elizabeth","Girls","Clean 135 lbs, Ring Dips",0,"21-15-9 reps For time");
			WodTable.addInitialWod(db, "Fran","Girls","Thruster 95 lbs, Pull-ups",0,"21-15-9 reps, For time");
			WodTable.addInitialWod(db, "Grace","Girls","Clean and Jerk 135 lbs",30,"For time");
			WodTable.addInitialWod(db, "Helen","Girls","400 meter run, 1.5 Pood Kettlebell swing x 21, Pull-ups (12 reps)",0,"3 rounds For time");
			WodTable.addInitialWod(db, "Isabel","Girls","Snatch 135 pounds",30,"For time");
			WodTable.addInitialWod(db, "Jackie","Girls","1000 meter row, Thruster 45 lbs (50 reps), Pull-ups (30 reps)",0,"For time");
			WodTable.addInitialWod(db, "Karen","Girls","Wall-ball 150 pounds",0,"For time");
			WodTable.addInitialWod(db, "Linda","Girls","Deadlift 1 1/2 BW, Bench BW, Clean 3/4 BW",0,"10/9/8/7/6/5/4/3/2/1 reps For time");
			WodTable.addInitialWod(db, "Mary","Girls","5 Handstand Push-ups, 10 1-legged squats, 15 Pull-ups",0,"As many rounds as possible in 20 min");
			WodTable.addInitialWod(db, "Nancy","Girls","400 meter run, Overhead squat 95 lbs x 15",0,"5 rounds For time");
			WodTable.addInitialWod(db, "Annie","Girls","Double-unders, Sit-ups",0,"50-40-30-20-10 reps per round, For time");
			WodTable.addInitialWod(db, "Eva","Girls","Run 500 meters, 2 Pood Kettlebell swing (30 reps), 30 Pull-ups",0,"5 rounds For time");
			WodTable.addInitialWod(db, "Kelly","Girls","Run 400 meters, 30 box jump (24 inch box), 30 Wall-ball shots (20 pound ball)",0,"5 rounds For time");
			WodTable.addInitialWod(db, "Lynne","Girls","Bodyweight bench press (e.g. same amount on bar as you weigh), Pull-ups",0,"5 rounds for max reps");
			WodTable.addInitialWod(db, "Nicole","Girls","Run 400 meters, Max rep Pull-ups",0,"As many rounds as possible in 20 min.( Note number of Pull-ups completed for each round)");
			WodTable.addInitialWod(db, "JT","Heroes","Handstand Push-ups, Ring dips, Push-ups",0,"21-15-9, For time");
			WodTable.addInitialWod(db, "Michael","Heroes","Run 800 meters, 50 Back Extensions",0,"3 rounds, For time");
			WodTable.addInitialWod(db, "Murph","Heroes","1 mile run, 100 Pull-ups, 200 Push-ups, 300 Squats, 1 mile run",0,"Partition the pull-ups, push-ups, and squats as needed. Start and finish with a mile run. If you've got a twenty pound vest or body armor, wear it. For time");
			WodTable.addInitialWod(db, "Daniel","Heroes","50 Pull-ups, 400 meter run, 95 pound Thruster (21 reps), 800 meter run, 95 pound Thruster (21 reps), 400 meter run, 50 Pull-ups",0,"For time");
			WodTable.addInitialWod(db, "Jason","Heroes","100 Squats, 5 Muscle-ups, 75 Squats, 10 Muscle-ups, 50 Squats, 15 Muscle-ups, 25 Squats, 20 Muscle-ups",0,"For time");
			WodTable.addInitialWod(db, "Badger","Heroes","95 pound Squat clean (30 reps), 30 Pull-ups, Run 800 meters",0,"3 rounds, For time");
			WodTable.addInitialWod(db, "Joshie","Heroes","40 pound Dumbbell snatct (21 reps) (right arm), 21 L Pull-ups 40 pound Dumbbell snatch (21 reps) (left arm), 21 L Pull-ups",0,"The snatches are full squat snatches. 3 rounds, For time");
			WodTable.addInitialWod(db, "Nate","Heroes","2 Muscle-ups, 4 Handstand Push-ups, 8 2-Pood Kettlebell swings",0,"As many rounds as possible in 20 minutes");
			WodTable.addInitialWod(db, "Randy","Heroes","75# power snatch",75,"For time");
			WodTable.addInitialWod(db, "Tommy V","Heroes","115 pound Thruster (21 reps), 15 ft Rope Climb (12 ascents), 115 pound Thruster (15 reps), 15 ft Rope Climb (9 ascents), 115 pound Thruster (9 reps), 15 ft Rope Climb (6 ascents)",0,"For time");
			WodTable.addInitialWod(db, "Griff","Heroes","Run 800 meters, Run 400 meters backwards, Run 800 meters, Run 400 meters backwards",0,"For time");
			WodTable.addInitialWod(db, "Ryan","Heroes","7 Muscle-ups, 21 Burpees",0,"Each burpee terminates with a jump one foot above max standing reach. 5 rounds, For time");
			WodTable.addInitialWod(db, "Erin","Heroes","40 pound Dumbbells split clean (15 reps), 21 Pull-ups",0,"5 rounds, For time");
			WodTable.addInitialWod(db, "Mr. Joshua","Heroes","Run 400 meters, 30 Glute-ham sit-ups, 250 pound Deadlift (15 reps)",0,"5 rounds, For time");
			WodTable.addInitialWod(db, "DT","Heroes","155 pound Deadlift (12 reps), 155 pound Hang power clean (9 reps), 155 pound Push jerk (6 reps)",0,"5 rounds, For time");
			WodTable.addInitialWod(db, "Danny","Heroes","24\" box jump (30 reps), 115 pound push press (20 reps), 30 pull-ups",0,"As many rounds in 20 min");
			WodTable.addInitialWod(db, "Hansen","Heroes","2 pood Kettlebell swing (30 reps), 30 Burpees, 30 Glute-ham sit-ups",0,"5 rounds, For time");
			WodTable.addInitialWod(db, "Tyler","Heroes","7 Muscle-ups, 95 pound Sumo-deadlift high-pull (21 reps) ",0,"5 rounds, For time");
			WodTable.addInitialWod(db, "Lumberjack 20","Heroes","20 Deadlifts (275lbs), Run 400 meters, 20 Kettlebell swings (2 pood), Run 400 meters, 20 Overhead Squats (115lbs), Run 400 meters, 20 Burpees, Run 400 meters, 20 Pullups (Chest to Bar), Run 400 meters, 20 Box jumps (24\"), Run 400 meters, 20 DB Squat Cleans (45lbs each), Run 400 meters",0,"For time");
			WodTable.addInitialWod(db, "Stephen","Heroes","GHD sit-up, Back extension, Knees to elbow, 95 pound Stiff legged deadlift",0,"30-25-20-15-10-5 rep rounds, For time");
			WodTable.addInitialWod(db, "Garrett","Heroes","75 Squats, 25 Ring handstand push-ups, 25 L-pull-ups",0,"3 rounds, For time");
			WodTable.addInitialWod(db, "War Frank","Heroes","25 Muscle-ups, 100 Squats, 35 GHD situps",0,"3 rounds, For time");
			WodTable.addInitialWod(db, "McGhee","Heroes","275 pound Deadlift (5 reps), 13 Push-ups, 9 Box jumps  (24\")",0,"rounds in 30 min");
			WodTable.addInitialWod(db, "Paul","Heroes","50 Double unders, 35 Knees to elbows, 185 pound Overhead walk (20 yards)",0,"5 rounds, For time");
			WodTable.addInitialWod(db, "Jerry","Heroes","Run 1 mile, Row 2K, Run 1 mile",0,"For time");
			WodTable.addInitialWod(db, "Nutts","Heroes","10 Handstand push-ups, 250 pound Deadlift (15 reps), 25 Box jumps (30\"), 50 Pull-ups, 100 Wallball shots (20 pounds) (10'), 200 Double-unders, Run 400 meters with a 45lb plate",0,"For time");
			WodTable.addInitialWod(db, "Arnie","Heroes","21 Turkish get-ups (Right arm), 50 Swings, 21 Overhead squats (Left arm), 50 Swings, 21 Overhead squats (Right arm), 50 Swings, 21 Turkish get-ups (Left arm)",0,"With a single 2 pood kettlebell, For time");
			WodTable.addInitialWod(db, "The Seven","Heroes","7 Handstand push-ups, 135 pound Thruster (7 reps), 7 Knees to elbows, 245 pound Deadlift (7 reps), 7 Burpees, 7 Kettlebell swings (2 pood), 7 Pull-ups",0,"7 rounds, For time");
			WodTable.addInitialWod(db, "RJ","Heroes","Run 800 meters, 15 ft Rope Climb (5 ascents), 50 Push-ups",0,"5 rounds, For time");
			WodTable.addInitialWod(db, "Luce","Heroes","Run 1K, 10 Muscle-ups, 100 Squats",0,"Wearing a 20 pound vest, 3 rounds, For time");
			WodTable.addInitialWod(db, "JOHNSON","Heroes","Clean and Jerk 135 lbs",0,"For time");
			WodTable.addInitialWod(db, "ROY","Heroes","225 pound Deadlift (15 reps), 20 Box jumps (24\"), 25 Pull-ups",0,"5 rounds, For time");
			WodTable.addInitialWod(db, "ADAMBROWN","Heroes","295 pound Deadlift (24 reps),24 Box jumps (24 inch box), 24 Wallball shots (20 pound ball1), 95 pound Bench press (24 reps), 24 Box jumps (24 inch box) 24 Wallball shots (20 pound ball), 145 pound Clean (24 reps)",0,"2 rounds, For time");
			WodTable.addInitialWod(db, "COE","Heroes","Clean and Jerk 135 lbs",0,"For time");
			WodTable.addInitialWod(db, "SEVERIN","Heroes","50 Strict Pull-ups, 100 Push-ups (release hands from floor at the bottom), Run 5K",0,"If you've got a twenty pound vest or body armor, wear it, For time");
			WodTable.addInitialWod(db, "HELTON","Heroes","Run 800 meters, 50 pound dumbbell squat cleans (30 reps), 30 Burpees",0,"3 rounds, For time");
			WodTable.addInitialWod(db, "JACK","Heroes","115 pound Push press (10 reps), 10 KB Swings (1.5 pood), 10 Box jumps (24 inch box)",0,"Max rounds in 20 min");
			WodTable.addInitialWod(db, "FORREST","Heroes","20 L-pull-ups, 30 Toes to bar, 40 Burpees, Run 800 meters",0,"3 rounds, For time");
			WodTable.addInitialWod(db, "BULGER","Heroes","Run 150 meters, 7 Chest to bar pull-ups, 135 pound Front squat (7 reps), 7 Handstand push-ups",0,"10 rounds, For time");
			WodTable.addInitialWod(db, "BRENTON","Heroes","Bear crawl 100 feet, Standing broad-jump (100 feet), Do three Burpees after every five broad-jumps",0," If you've got a twenty pound vest or body armor, wear it, 5 rounds, For time");
			WodTable.addInitialWod(db, "BLAKE","Heroes","100 foot Walking lunge with 45lb plate held overhead, 30 Box jump (24 inch box), 20 Wallball shots (20 pound ball), 10 Handstand push-ups",0,"4 rounds, For time");
			WodTable.addInitialWod(db, "COLIN","Heroes","Carry 50 pound sandbag 400 meters, 115 pound Push press (12 reps), 12 Box jumps (24 inch box), 95 pound Sumo deadlift high-pull (12 reps)",0,"6 rounds, For time");
			WodTable.addInitialWod(db, "THOMPSON","Heroes","15 ft Rope Climb (1 ascent), 95 pound Back squat (29 reps), 135 pound barbells Farmer carry (10 meters)",0,"Begin the rope climbs seated on the floor, 10 rounds, For time");
			WodTable.addInitialWod(db, "WHITTEN","Heroes","22 Kettlebell swings (2 pood), 22 Box jump (24 inch box), Run 400 meters, 22 Burpees, 22 Wall ball shots (20 pound ball)",0,"5 rounds, For time");
			WodTable.addInitialWod(db, "BULL","Heroes","200 Double-unders, 135 pound Overhead squat (50 reps), 50 Pull-ups, Run 1 mile",0,"2 rounds, For time");
			WodTable.addInitialWod(db, "HOLBROOK","Heroes","115 pound Thruster (5 reps), 10 Pull-ups, 100 meter Sprint, Rest 1 minute",0,"10 rounds, For time");
			WodTable.addInitialWod(db, "LEDESMA","Heroes","5 Parallette handstand push-ups, 10 Toes through rings, 20 pound Medicine ball cleans (15 reps)",0,"As many rounds as possible in 20 min");
			WodTable.addInitialWod(db, "WITTMAN","Heroes","1.5 pood Kettlebell swing (15 reps), 95 pound Power clean (15 reps), 15 Box jumps, (24 inch box)",0,"7 rounds, For time");
			WodTable.addInitialWod(db, "MCCLUSKEY","Heroes","9 Muscle-ups, 15 Burpee pull-ups, 21 Pull-ups, Run 800 meters",0,"3 rounds, For time");
			WodTable.addInitialWod(db, "WEAVER","Heroes","10 L-pull-ups, 15 Push-ups, 15 Chest to bar Pull-ups, 15 Push-ups, 20 Pull-ups, 15 Push-ups",0,"4 rounds, For time");
			WodTable.addInitialWod(db, "ABBATE","Heroes","Run 1 mile, 155 pound Clean and jerk (21 reps), Run 800 meters, 155 pound Clean and jerk (21 reps), Run 1 Mile",0,"For time");
			WodTable.addInitialWod(db, "HAMMER","Heroes","135 pound Power clean (5 reps), 135 pound Front squat (10 reps), 135 pound Jerk (5 reps), 20 Pull-ups",0,"Rest 90 seconds between each round, 5 rounds, For time");
			WodTable.addInitialWod(db, "MOORE","Heroes","15 ft Rope Climb (1 ascent), Run 400 meters, Max rep Handstand push-ups",0,"As many rounds as possible in 20 min");
			WodTable.addInitialWod(db, "WILMOT","Heroes","50 Squats, 25 Ring dips",0,"6 rounds, For time");
			WodTable.addInitialWod(db, "MOON","Heroes","40 pound dumbbell Hang split snatch (10 reps)(Right arm1), 5 ft Rope Climb (1 ascent), 40 pound dumbbell Hang split snatch (10 reps) (Left arm), 15 ft Rope Climb (1 ascent)",0,"Alternate feet in the split snatch sets, 7 rounds, For time");
			WodTable.addInitialWod(db, "SMALL","Heroes","Row 1000 meters, 50 Burpees, 50 Box jumps (24 inch box), Run 800 meters",0,"3 rounds, For time");
			
			
		
			
	}
}