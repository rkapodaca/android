package com.Ripped.Control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.R;
import android.content.Context;
import android.util.Log;
import android.widget.ExpandableListAdapter;
import android.widget.SimpleExpandableListAdapter;

public class QnA {
	private static final String NAME = "NAME";
    private static final String IS_EVEN = "IS_EVEN";
    private static ExpandableListAdapter mAdapter;
    
	public static ExpandableListAdapter display2(Context ctx){
		
		List<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
        List<List<Map<String, String>>> childData = new ArrayList<List<Map<String, String>>>();
   
            Map<String, String> curGroupMap = new HashMap<String, String>();
            groupData.add(curGroupMap);
            curGroupMap.put(NAME, "QnA # 1");
            //curGroupMap.put(IS_EVEN, (i % 2 == 0) ? "ctx group is even" : "ctx group is odd");
            
            List<Map<String, String>> children = new ArrayList<Map<String, String>>();
           
                Map<String, String> curChildMap = new HashMap<String, String>();
                curChildMap.put(IS_EVEN,"What Should I Eat?\n"+
"In plain language, base your diet on garden vegetables, especially greens, lean meats, nuts and seeds, little starch, and no sugar. That's about as simple as we can get. Many have observed that keeping your grocery cart to the perimeter of the grocery store while avoiding the aisles is a great way to protect your health. Food is perishable. The stuff with long shelf life is all suspect. If you follow these simple guidelines you will benefit from nearly all that can be achieved through nutrition.");
                children.add(curChildMap);
               // curChildMap.put(NAME, "Child " + j);
               
            
            childData.add(children);
            
        
        // Set up our adapter
        mAdapter = new SimpleExpandableListAdapter(
                ctx,
                groupData,
                android.R.layout.simple_expandable_list_item_1,
                new String[] { NAME, IS_EVEN },
                new int[] { android.R.id.text1, android.R.id.text2,},
                childData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] { NAME, IS_EVEN },
                new int[] { android.R.id.text1, android.R.id.text2,});
                
              /*  0,
                null,
                new int[] {})(
                {
                    @Override
                    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
                        final View v = super.getChildView(groupPosition, childPosition, isLastChild, convertView, parent);

                        // Populate your custom view here
                        ((TextView)v.findViewById(R.id.name)).setText( (String) ((Map<String,Object>)getChild(groupPosition, childPosition)).get(NAME) );
                        ((ImageView)v.findViewById(R.id.image)).setImageDrawable( (Drawable) ((Map<String,Object>)getChild(groupPosition, childPosition)).get(IMAGE) );

                        return v;
                    }

                    @Override
                    public View newChildView(boolean isLastChild, ViewGroup parent) {
                         return layoutInflater.inflate(R.layout.expandable_list_item_with_image, null, false);
                    }
                });*/
    		
        
        Log.d("archive", "is this working?");
        return mAdapter;
	}
	
}

