package com.Ripped.Model;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class WodTable extends DBadapter {
	  
	
	public WodTable(Context context) {
		super(context);
	}

	    // Adding new wod
		public void addWod(String wodname, String typwod, String set_struct, int repsForTime,String timeToFinishReps) {
			SQLiteDatabase db = this.getWritableDatabase();
			
			ContentValues values = new ContentValues();
			//values.put(KEY_ROWID, wod.getwodname()); 
			values.put(KEY_WOD_NAME, wodname);
			values.put(KEY_TYPWOD, typwod);
			values.put(KEY_SETSTRUCT, set_struct);
			values.put(KEY_REPFORTIME, repsForTime);
			values.put(KEY_TIMEFORFINISHREPS, timeToFinishReps);
			
			
			// Inserting Row
			db.insert(TABLE_WODS, null, values);
			db.close(); // Closing database connection
		}
		
		public static void addInitialWod(SQLiteDatabase db, String wodname, String typwod, 
				String set_struct, int repsForTime, String timeToFinishReps) {
			 			
			ContentValues values = new ContentValues();
			//values.put(KEY_ROWID, wod.getwodname()); 
			values.put(KEY_WOD_NAME, wodname);
			values.put(KEY_TYPWOD, typwod);
			values.put(KEY_SETSTRUCT, set_struct);
			values.put(KEY_REPFORTIME, repsForTime);
			values.put(KEY_TIMEFORFINISHREPS, timeToFinishReps);
			
			
			// Inserting Row
			db.insert(TABLE_WODS, null, values);
		}
	    
	    	 	
	    public ArrayList<Wod> getAllWods() {
	    	ArrayList<Wod> WodList = new ArrayList<Wod>();
	    	// Select All Query
	    	String selectQuery = "SELECT * FROM " + TABLE_WODS;
	    	
	    	SQLiteDatabase db = this.open();
	    	Cursor cursor = db.rawQuery(selectQuery, null);
	    	
	    		
	    	// looping through all rows and adding to list
	    	Wod wod;
	    	if (cursor.moveToFirst()) {
	    	
	    		do {
	    			
	    			wod= new Wod( cursor.getString(1), cursor.getString(2), 
	    					cursor.getString(3), Integer.parseInt(cursor.getString(4)), cursor.getString(5));
	    			// Adding contact to list
	    			WodList.add(wod);
	    		} while (cursor.moveToNext());
	    	}
	    db.close();
	    // return contact list
	   	return WodList;
	    } 
	    
	    
	    public void printAllWods() {
	
	    	// Select All Query
	    	String selectQuery = "SELECT * FROM " + TABLE_WODS;
	    	
	    	SQLiteDatabase db = this.open();
	    	Cursor cursor = db.rawQuery(selectQuery, null);
	    	
	    		
	    	// looping through all rows and adding to list
	    	
	    	if (cursor.moveToFirst()) {
	    		
	    		do {
	    			
	    			Log.d("Wods", cursor.getString(1) + cursor.getString(2)
	    				+ cursor.getString(3) + cursor.getString(4) + cursor.getString(5));
	    			
	    		} while (cursor.moveToNext());
	    	}
	    db.close();
	    
	   	
	    }
	    
	    
	    
		public ArrayList<Wod> getGirlsWODS(){
			ArrayList<Wod> girlsWODS = new ArrayList<Wod>();
			String selectQuery = "SELECT * FROM " + TABLE_WODS +
					" where " + KEY_TYPWOD + " = 'Girls'";
			
			SQLiteDatabase db = this.open();
			Cursor cursor = db.rawQuery(selectQuery, null);
			Wod gw = new Wod();
			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
			do {
				gw.setName( cursor.getString(1));
				gw.setType(cursor.getString(2));
				gw.setInstruction(cursor.getString(3));
				gw.setReps(Integer.parseInt(cursor.getString(4)));
				gw.setTime(cursor.getString(5));
				// Adding contact to list
				girlsWODS.add(gw);
			} while (cursor.moveToNext());
		}
		db.close();
		// return contact list
		return girlsWODS;
		}
		
		
		public ArrayList<Wod> getHeroesWODS(){
			ArrayList<Wod> heroesWODS = new ArrayList<Wod>();
			String selectQuery = "SELECT * FROM " + TABLE_WODS +
					" where " + KEY_TYPWOD + " = 'Heroes'";
			
			SQLiteDatabase db = this.open();
			Cursor cursor = db.rawQuery(selectQuery, null);
			Wod hw = new Wod();
			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
			do {
				hw.setName( cursor.getString(1));
				hw.setType(cursor.getString(2));
				hw.setInstruction(cursor.getString(3));
				hw.setReps(Integer.parseInt(cursor.getString(4)));
				hw.setTime(cursor.getString(5));
				// Adding contact to list
				heroesWODS.add(hw);
			} while (cursor.moveToNext());
		}
		db.close();
		// return contact list
		return heroesWODS;
		}
}

