package org.kp.sdcatapp2Droid.db;

import java.util.ArrayList;
import java.util.Calendar;

public class PrepopulatedData {
	private static ArrayList<Event> pastEvents;
	private static ArrayList<Event> todaysEvents;

	/**
	 * Get events from the past.
	 */
	public static ArrayList<Event> getPastEvents(){
		return pastEvents;
	}

	/**
	 * Get today's events.
	 */
	public static ArrayList<Event> getTodaysEvents(){
		return todaysEvents;
	}

	/**
	 * Call this to popualte Margaret Thompson's information.
	 */
	public static void prepopulateData(){
		prepopulatePast();
		prepopulateToday();
	}
	
	public static void prepopulatePast(){
		if (pastEvents != null)
			return;
		
		Calendar cal = Calendar.getInstance();
		pastEvents = new ArrayList<Event>();
	
		// 3 years ago
		cal.set(2009, 3, 17, 13, 42);
		pastEvents.add(new Event("Check In", Event.CHECKING_IN_EVENT, "Patti Carothers", cal.getTime(), 2.25));
		cal.set(2009, 3, 17, 16, 7);
		pastEvents.add(new Event("Blood Test", Event.BLOOD_TEST_EVENT, "Erin Smith", cal.getTime(), 1.0));
		
		// 2 days ago
		cal.set(2012, 10, 25, 14, 30);
		pastEvents.add(new Event("Check In", Event.CHECKING_IN_EVENT, "Patti Carothers", cal.getTime(), 0.75));
		
		cal.set(Calendar.HOUR_OF_DAY, 15);
		cal.set(Calendar.MINUTE, 20);
		pastEvents.add(new Event("Doctor Visit", Event.DOCTOR_VISIT, "Ryan Harris", cal.getTime(), 1.0));
		
		cal.set(Calendar.HOUR_OF_DAY, 16);
		cal.set(Calendar.MINUTE, 45);
		pastEvents.add(new Event("Heart Surgery", Event.SURGERY_EVENT, "Ryan Harris", cal.getTime(), 4.5));
		
		// yesterday
		cal.set(2012, 10, 26, 9, 0);
		pastEvents.add(new Event("Oral Care", Event.HYGIENE_EVENT, "Jeff Tsai", cal.getTime(), 0.2));
		
		cal.set(Calendar.HOUR_OF_DAY, 9);
		cal.set(Calendar.MINUTE, 20);
		pastEvents.add(new Event("Medicine", Event.MEDICATION_EVENT, "Jeff Tsai", cal.getTime(), 0.2));
		
		cal.set(Calendar.HOUR_OF_DAY, 9);
		cal.set(Calendar.MINUTE, 40);
		pastEvents.add(new Event("Breakfast", Event.MEAL_EVENT, "Jeff Tsai", cal.getTime(), 1.0));
		
		cal.set(Calendar.HOUR_OF_DAY, 12);
		cal.set(Calendar.MINUTE, 0);
		pastEvents.add(new Event("Lunch", Event.MEAL_EVENT, "Sharron Lewis", cal.getTime(), 1.0));
		
		cal.set(Calendar.HOUR_OF_DAY, 14);
		cal.set(Calendar.MINUTE, 0);
		pastEvents.add(new Event("Nurse Check Up", Event.CHECK_UP_EVENT, "Sharron Lewis", cal.getTime(), 0.5));
		
		cal.set(Calendar.HOUR_OF_DAY, 16);
		cal.set(Calendar.MINUTE, 43);
		pastEvents.add(new Event("Nurse Check Up", Event.CHECK_UP_EVENT, "Jeff Tsai", cal.getTime(), 0.5));
		
		cal.set(Calendar.HOUR_OF_DAY, 18);
		cal.set(Calendar.MINUTE, 0);
		pastEvents.add(new Event("Visitors", Event.VISITING_EVENT, "Kayla Thompson, Andrew Thompson", cal.getTime(), 2.0));
		
		cal.set(Calendar.HOUR_OF_DAY, 17);
		cal.set(Calendar.MINUTE, 50);
		pastEvents.add(new Event("Medicine", Event.MEDICATION_EVENT, "Sharron Lewis", cal.getTime(), 0.1));
		
		cal.set(Calendar.HOUR_OF_DAY, 18);
		cal.set(Calendar.MINUTE, 0);
		pastEvents.add(new Event("Dinner", Event.MEAL_EVENT, "Sharron Lewis", cal.getTime(), 1.0));
		
		cal.set(Calendar.HOUR_OF_DAY, 20);
		cal.set(Calendar.MINUTE, 45);
		pastEvents.add(new Event("Bath", Event.HYGIENE_EVENT, "Sharron Lewis", cal.getTime(), 1.0));
		
		cal.set(Calendar.HOUR_OF_DAY, 22);
		cal.set(Calendar.MINUTE, 0);
		pastEvents.add(new Event("Nurse Check Up", Event.CHECK_UP_EVENT, "Jeff Tsai", cal.getTime(), 0.2));
	
		// today in the past
		cal.set(2012, 10, 27, 9, 0);
		pastEvents.add(new Event("Oral Care", Event.HYGIENE_EVENT, "Jeff Tsai", cal.getTime(), 0.2));
		
		cal.set(Calendar.HOUR_OF_DAY, 9);
		cal.set(Calendar.MINUTE, 20);
		pastEvents.add(new Event("Medicine", Event.MEDICATION_EVENT, "Jeff Tsai", cal.getTime(), 0.1));
		
		cal.set(Calendar.HOUR_OF_DAY, 9);
		cal.set(Calendar.MINUTE, 35);
		pastEvents.add(new Event("Breakfast", Event.MEAL_EVENT, "Jeff Tsai", cal.getTime(), 1.0));
		
		cal.set(Calendar.HOUR_OF_DAY, 12);
		cal.set(Calendar.MINUTE, 0);
		pastEvents.add(new Event("Lunch", Event.MEAL_EVENT, "Sharron Lewis", cal.getTime(), 1.0));
		
		cal.set(Calendar.HOUR_OF_DAY, 14);
		cal.set(Calendar.MINUTE, 40);
		pastEvents.add(new Event("Nurse Check Up", Event.CHECK_UP_EVENT, "Jeff Tsai", cal.getTime(), 0.2));
	}
	
	public static void prepopulateToday(){
		if (todaysEvents != null)
			return;
		Calendar cal = Calendar.getInstance();
		todaysEvents = new ArrayList<Event>();
		// today
		cal.set(2012, 10, 27, 18, 0);
		todaysEvents.add(new Event("Dinner", Event.MEAL_EVENT, "Sharron Lewis", cal.getTime(), 1.0));
		
		cal.set(Calendar.HOUR_OF_DAY, 19);
		cal.set(Calendar.MINUTE, 30);
		todaysEvents.add(new Event("Doctor Visit", Event.DOCTOR_VISIT, "Ryan Harris", cal.getTime(), 1.0));
	}
}
