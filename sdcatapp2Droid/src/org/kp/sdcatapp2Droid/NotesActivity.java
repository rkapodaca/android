package org.kp.sdcatapp2Droid;

import org.kp.sdcatapp2Droid.db.Event;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class NotesActivity extends Activity 
{
	private static Event last_click_event = MainActivity.lastClickEvent;
	private EditText notesTxt;
	private TextView notesDetails;
	private Button btnConfirm,btnCancel;
	
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notes);
        
        notesTxt = (EditText)findViewById(R.id.editText_notes);
        notesDetails = (TextView)findViewById(R.id.textView_notes);
        btnConfirm = (Button)findViewById(R.id.btn_notesConfirm);
        btnCancel = (Button)findViewById(R.id.btn_notesCancel);
        
        notesDetails.setText(last_click_event.getName() + "\nProcedure time: " + last_click_event.getDuration() + "\nPractitioner: " + last_click_event.getPersonInvolved());
        
        this.setupListeners();
    }

	// Sets up event listeners
	private void setupListeners()
	{
		// Button Confirm listener
    	btnConfirm.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View view) 
			{
				
				last_click_event.setNotes(notesTxt.getText().toString());
				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent); 
			}
		});
    	
    	
    	btnCancel.setOnClickListener(new View.OnClickListener() {
    		
    		public void onClick(View view){
    			Intent intent = new Intent(getApplicationContext(), MainActivity.class);
    			
    			startActivity(intent);
    		}
    	
    	
    	});
	}
}
