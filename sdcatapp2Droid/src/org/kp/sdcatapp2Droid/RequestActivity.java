package org.kp.sdcatapp2Droid;

import org.kp.sdcatapp2Droid.db.Event;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;


public class RequestActivity extends Activity
{

	private TimePicker timePicker;
	
	private Button btnCancel, btnConfirm;
	
	private EditText et_request;
	private TextView requestDetails; 
	private static Event last_click_event = MainActivity.lastClickEvent;

	
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request);
        
        requestDetails = (TextView)findViewById(R.id.textView_request);
        timePicker = (TimePicker)findViewById(R.id.timePicker1);
        btnCancel = (Button)findViewById(R.id.btn_cancelRequest);
        btnConfirm = (Button)findViewById(R.id.btn_confirm);
        et_request = (EditText)findViewById(R.id.editText1);
        
        
        requestDetails.setText(last_click_event.getName() + "\nProcedure time: " + last_click_event.getDuration() + "\nPractitioner: " + last_click_event.getPersonInvolved());
        
        
        this.setupListeners();
        
    }
	
	private void setupListeners()
	{
    	btnConfirm.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View view) {
				
				// Change the event
				//MainActivity.lastClickEvent.
				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent); 
                
				
			}
		});
    	
    	btnCancel.setOnClickListener(new View.OnClickListener() {
    		
    		public void onClick(View view){
    			Intent intent = new Intent(getApplicationContext(), MainActivity.class);
    			startActivity(intent);
    		}
    	});
	}
}
