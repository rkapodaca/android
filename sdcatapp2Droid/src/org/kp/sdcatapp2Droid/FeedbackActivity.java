package org.kp.sdcatapp2Droid;

import org.kp.sdcatapp2Droid.db.Event;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class FeedbackActivity extends Activity 
{
	private TextView txtEvent, txtNotes;
	private Button btnConfirm, btnCancel;
	private EditText et_feedback;
	private RadioGroup ratingRadio;
	private RadioButton btn1, btn2, btn3, btn4, btn5;
	private static Event last_click_event = MainActivity.lastClickEvent;


	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback);

		txtEvent = (TextView)findViewById(R.id.textView1);
		txtNotes = (TextView)findViewById(R.id.textView2);
		et_feedback = (EditText)findViewById(R.id.editText1);
		ratingRadio = (RadioGroup)findViewById(R.id.radioGroup1);
		btnConfirm = (Button)findViewById(R.id.btnConfirmFeed);
		btnCancel = (Button)findViewById(R.id.btnCancelFeed);
		btn1 = (RadioButton)findViewById(R.id.radio0);
		btn2 = (RadioButton)findViewById(R.id.radio1);
		btn3 = (RadioButton)findViewById(R.id.radio2);
		btn4 = (RadioButton)findViewById(R.id.radioButton1);

		this.setupListeners();
	}

	public void onResume()
	{
		super.onResume();
		
		txtEvent.setText(last_click_event.getName() + "\nProcedure time: " + last_click_event.getDuration() + "\nPractitioner: " + last_click_event.getPersonInvolved());
		txtNotes.setText("Notes: "+ last_click_event.getNotes() );
		if (MainActivity.lastClickEvent.getFeedback() != null)
		{
			et_feedback.setText(MainActivity.lastClickEvent.getFeedback().getDetails()); 
			switch(MainActivity.lastClickEvent.getFeedback().getRate())
			{
				case 0:
					btn1.setChecked(true);
					btn2.setChecked(false);
					btn3.setChecked(false);
					btn4.setChecked(false);
					break;
				case 1:
					btn1.setChecked(false);
					btn2.setChecked(true);
					btn3.setChecked(false);
					btn4.setChecked(false);
					break;
				case 2:
					btn1.setChecked(false);
					btn2.setChecked(false);
					btn3.setChecked(true);
					btn4.setChecked(false);
					break;
				case 3:
					btn1.setChecked(false);
					btn2.setChecked(false);
					btn3.setChecked(false);
					btn4.setChecked(true);
					break;
			}
		}
	}
	
	// Sets up click listeners
	private void setupListeners()
	{
		// Button Confirm listener
		btnConfirm.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {
				int vote = -1;
				if(btn1.isChecked())
					vote = 0;
				if(btn2.isChecked())
					vote = 1;
				if(btn3.isChecked())
					vote = 3;
				if(btn4.isChecked())
					vote = 4;

				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
				startActivity(intent); 
			}
		});

		// Button Cancel Listener
		btnCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view){
				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
				startActivity(intent);
			}
		});
	}
}
