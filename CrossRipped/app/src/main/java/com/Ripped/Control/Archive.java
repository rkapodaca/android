package com.Ripped.Control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.Ripped.CrossRipped.R;

import android.content.Context;
import android.util.Log;
import android.widget.ExpandableListAdapter;
import android.widget.SimpleExpandableListAdapter;

import com.Ripped.Model.Wod;

public class Archive {
	private static final String NAME = "NAME";
    private static final String IS_EVEN = "IS_EVEN";
    private static ExpandableListAdapter mAdapter; 
    
	public static ExpandableListAdapter displayDemGirls(Context ctx){
	
		List<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
        List<List<Map<String, String>>> childData = new ArrayList<List<Map<String, String>>>();
        
        ListOfWODS girls = new ListOfWODS(ctx);
       
       	LinkedList<String>  girl = new LinkedList<String>();
       	
       	String singleChild;
          	
        	girl = girls.getGirlName();
        	String girlName = "";
        	while(girl.peekFirst() != null){
        		//Getting and Setting the Names of the Girls WODs
        		Map<String, String> curGroupMap = new HashMap<String, String>();
            	groupData.add(curGroupMap);
            	girlName = girl.pop();
        		curGroupMap.put(NAME, girlName);
 

				List<Map<String, String>> children = new ArrayList<Map<String, String>>();
				LinkedList<String> child = girls.getGirlInstructions(girlName);
	            while (child.peekFirst() != null) {
	                Map<String, String> curChildMap = new HashMap<String, String>();
	                children.add(curChildMap);
	                curChildMap.put(NAME, child.pollFirst());
	            }
	            childData.add(children);
	            
        	}
        
        
        	        mAdapter = new SimpleExpandableListAdapter(
                    ctx,
                    groupData,
                    android.R.layout.simple_expandable_list_item_1,
                    new String[] { NAME, IS_EVEN },
                    new int[] { android.R.id.text1, android.R.id.text2 },
                    childData,
                    android.R.layout.simple_expandable_list_item_2,
                    new String[] { NAME, IS_EVEN },
                    new int[] { android.R.id.text1, android.R.id.text2 }
                    );
        
        return mAdapter;
        
	}
	
	public static ExpandableListAdapter displayDemHeroes(Context ctx){
		
		List<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
        List<List<Map<String, String>>> childData = new ArrayList<List<Map<String, String>>>();
        
        ListOfWODS heroes = new ListOfWODS(ctx);
       
       	LinkedList<String>  hero = new LinkedList<String>();
       	
       	String singleChild;
          	
        	hero = heroes.getHeroName();
        	String heroName = "";
        	while(hero.peekFirst() != null){
        		//Getting and Setting the Names of the Heroes WODs
        		Map<String, String> curGroupMap = new HashMap<String, String>();
            	groupData.add(curGroupMap);
            	heroName = hero.pop();
        		curGroupMap.put(NAME, heroName);
 
        		//Getting and Setting the sets of a Heroes
				List<Map<String, String>> children = new ArrayList<Map<String, String>>();
				LinkedList<String> child = heroes.getHeroInstructions(heroName);
	            while (child.peekFirst() != null) {
	                Map<String, String> curChildMap = new HashMap<String, String>();
	                children.add(curChildMap);
	                curChildMap.put(NAME, child.pollFirst());
	            }
	            childData.add(children);
	            
        	}
        
        
        	        mAdapter = new SimpleExpandableListAdapter(
                    ctx,
                    groupData,
                    android.R.layout.simple_expandable_list_item_1,
                    new String[] { NAME, IS_EVEN },
                    new int[] { android.R.id.text1, android.R.id.text2 },
                    childData,
                    android.R.layout.simple_expandable_list_item_2,
                    new String[] { NAME, IS_EVEN },
                    new int[] { android.R.id.text1, android.R.id.text2 }
                    );
        
        return mAdapter;
        
	}
	
	
	
	
	public static ExpandableListAdapter displayDemCustom(Context ctx){
   		

		
		List<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
        List<List<Map<String, String>>> childData = new ArrayList<List<Map<String, String>>>();
        
        ListOfWODS customWods = new ListOfWODS(ctx);
       
       	LinkedList<String>  custom = new LinkedList<String>();
       	
       	String singleChild;

        	custom = customWods.getCustomName();
        	
        	String customName = "";
        	while(custom.peekFirst() != null){
        		//Getting and Setting the Names of the Custom WODs
        		Map<String, String> curGroupMap = new HashMap<String, String>();
            	groupData.add(curGroupMap);
            	customName = custom.pop();
        		curGroupMap.put(NAME, customName);
 

				List<Map<String, String>> children = new ArrayList<Map<String, String>>();
				LinkedList<String> child = customWods.getCustomInstructions(customName);
	            while (child.peekFirst() != null) {
	                Map<String, String> curChildMap = new HashMap<String, String>();
	                children.add(curChildMap);
	                curChildMap.put(NAME, child.pollFirst());
	            }
	            childData.add(children);
	            
        	}
        
        
        	        mAdapter = new SimpleExpandableListAdapter(
                    ctx,
                    groupData,
                    android.R.layout.simple_expandable_list_item_1,
                    new String[] { NAME, IS_EVEN },
                    new int[] { android.R.id.text1, android.R.id.text2 },
                    childData,
                    android.R.layout.simple_expandable_list_item_2,
                    new String[] { NAME, IS_EVEN },
                    new int[] { android.R.id.text1, android.R.id.text2 }
                    );
        
        return mAdapter;
        
	}
	
	
	
}
