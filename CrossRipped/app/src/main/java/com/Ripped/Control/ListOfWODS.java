package com.Ripped.Control;

import java.util.ArrayList;
import java.util.LinkedList;

import android.content.Context;
import android.util.Log;

import com.Ripped.Model.CustomWod;
import com.Ripped.Model.CustomWodTable;
import com.Ripped.Model.Wod;
import com.Ripped.Model.WodTable;

public class ListOfWODS {
	
	private WodTable db;
	private CustomWodTable customDb;
	private ArrayList<Wod> allWods;
	private ArrayList<CustomWod> customWods;

	public ListOfWODS(Context ctx){
   		
 
		db = new WodTable(ctx);
		customDb = new CustomWodTable(ctx);

		allWods = db.getAllWods();
		customWods = customDb.getAllCustomWods();

	}

	public LinkedList<String> getGirlName(){
		LinkedList<String> girlWodName = new LinkedList<String>();
		
		for( Wod name: allWods)
		{
			
			if(name.getType().equals("Girls"))
			{
				girlWodName.add(name.getName());
			}
				
		}
		
		return girlWodName;
	}
	
	public LinkedList<String> getGirlInstructions(String girlName){
		LinkedList<String> girlWodSet = new LinkedList<String>();
		String fullInstrs = "";
		String[] singleInstr = null;


		for( Wod set: allWods)
		{
			if(set.getType().equals("Girls") && set.getName().equals(girlName))
			{
				fullInstrs = set.getInstruction();
				singleInstr = fullInstrs.split(",\\s*" );
				for(int i = 0; i < singleInstr.length; i++)
					girlWodSet.add(singleInstr[i]);
			}
		}

		
		return girlWodSet;		
	}
	
	public LinkedList<String> getHeroName(){
		LinkedList<String> HeroeWodName = new LinkedList<String>();
		
		for( Wod name: allWods)
		{
			if(name.getType().equals("Heroes"))
				HeroeWodName.add(name.getName());
		}
		
		return HeroeWodName;
	}
	
	
	
	public LinkedList<String> getHeroInstructions(String heroName){
		LinkedList<String> heroWodSet = new LinkedList<String>();
		String fullInstrs = "";
		String[] singleInstr = null;


		for( Wod set: allWods)
		{
			if(set.getType().equals("Heroes") && set.getName().equals(heroName))
			{
				fullInstrs = set.getInstruction();
				singleInstr = fullInstrs.split(",\\s*" );
				for(int i = 0; i < singleInstr.length; i++)
					heroWodSet.add(singleInstr[i]);
			}
		}

		
		return heroWodSet;		
	}
	
	
	
	
	
	

	
	public LinkedList<String> getCustomName(){
		LinkedList<String> customWodName = new LinkedList<String>();
		
		for( CustomWod name: customWods){
				customWodName.add(name.getName());
				Log.d("ListOfWods", name.getName());
		}
		
		return customWodName;
	}
	
	
	
	public LinkedList<String> getCustomInstructions(String customName){
		LinkedList<String> customWodSet = new LinkedList<String>();
		String fullInstrs = "";
		String[] singleInstr = null;


		for( CustomWod set: customWods )
		{
			if(customName.equals(set.getName())){
				fullInstrs = set.getInstruction();
				Log.d("ListOfWods-INSTRUCTIONS", set.getInstruction() );
				singleInstr = fullInstrs.split(",\\s*" );
				for(int i = 0; i < singleInstr.length; i++)
					customWodSet.add(singleInstr[i]);
			}
		}

		
		return customWodSet;		
	}
	
	
	/*
	public LinkedList<String> getGirl(Context ctx){
		WodTable db = new WodTable(ctx);
		//ArrayList<Wod> girlWods = db.getGirlsWODS();
		LinkedList<String> girlWodName = new LinkedList();

		for( Wod name: girlWods)
			girlWodName.add(name.getName());
		
		return girlWodName;
	}
	*/
/*		
	public LinkedList<String> getchildList(int i){
		LinkedList<String> child = new LinkedList();
		switch(i){
		case 1: //Angie
			child.add("100 Pull-ups");
			child.add("100 Push-ups");
			child.add("100 Sit-ups");
			child.add("100 Squats");
			break;
		case 2: //Barbara
			child.add("20 Pull-ups");
			child.add("30 Push-ups");
			child.add("40 Sit-ups");
			child.add("50 Squats");
			break;
		case 3: //Chelsea
			child.add("5 Pull-ups");
			child.add("10 Push-ups");
			child.add("15 Sit-ups");
			break;
		case 4: //Cindy
			child.add("5 Pull-ups");
			child.add("10 Push-ups");
			child.add("15 Sit-ups");
			break;
		case 5: //Diane
			child.add("Deadlift 225 lbs");
			child.add("Handstand push-ups");
			break;
		case 6: //Elizabeth
			child.add("Clean 135 lbs");
			child.add("Ring Dips");
			break;
		case 7: //Fran
			child.add("Thruster 95 lbs");
			child.add("Pul-ups");
			break;
		case 8: //Grace
			child.add("Clean and Jerk 135 lbs");
			break;
		case 9: //Helen
			child.add("400 meter run");
			child.add("1.5 pood Kettlebell swing x 21");
			child.add("Pull-ups 12 reps");
			break;
		case 10: //Isabel
			child.add("Snatch 135 pounds");
			break;
		case 11: //Jackie
			child.add("1000 meter row");
			child.add("Thruster 45 lbs (50 reps)");
			child.add("Pull-ups (30 reps)");
			break;
		case 12: //Karen
			child.add("Wall-ball 150 shots");
			break;
		case 13: //Linda
			child.add("Deadlift 1 1/2 BW");
			child.add("Bench BW");
			child.add("Clean 3/4 BW");
			break;
		case 14: //Mary
			child.add("5 Handstand push-ups");
			child.add("10 1-legged squats");
			child.add("15 Pull-ups");
			break;
		case 15: //Nancy
			child.add("400 meter run");
			child.add("Overhead squat 95 lbs x 15 ");
			break;
			
		}
		return child;
		
	}

public String getTitle(int i){
	
	String title = "";
	switch(i){
	case 1: title = "Angie";
	break;
		
		
	case 2: title = "Barbara";
	break;
		
	case 3: title = "Chelsea";
	break;
		
	case 4: title = "Cindy";
	break;
		
	case 5: title = "Diane";
	break;
		
	case 6: title = "Elizabeth";
	break;
		
	case 7: title = "Fran";
	break;
		
	case 8: title = "Grace";
	break;
		
	case 9: title = "Helen";
	break;
		
	case 10: title = "Isabel";
	break;
	
	case 11: title = "Jackie";
	break;
		
	case 12: title ="Karen";
	break;
		
	case 13: title = "Linda";
	break;
		
	case 14: title = "Mary";
	break;
		
	case 15: title = "Nancy";
	break;
		
	}
	return title;
	
}
*/

}
