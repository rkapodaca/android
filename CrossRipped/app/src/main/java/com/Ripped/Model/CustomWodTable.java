package com.Ripped.Model;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CustomWodTable extends DBadapter {
	
	public CustomWodTable(Context context) {
		super(context);
	}
/*	
	// Creating Table
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_GENWOD_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_CUSTOM + "("
	   	+ KEY_ROWID + " INTEGER PRIMARY KEY, " 
	   	+ KEY_CUST_NAME+ " TEXT, "  
	   	+ KEY_TIMETOFINISHWOD + " TEXT, "
	   	+ KEY_WODSTRUCT + " TEXT);";	
		db.execSQL(CREATE_GENWOD_TABLE);
	}
*/
	
	// Adding new exercise
	public void addCustomWod(String name, String time, String struct ) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(KEY_CUST_NAME, name); 
		values.put(KEY_TIMETOFINISHWOD, time);
		values.put(KEY_WODSTRUCT, struct);

		
		// Inserting Row
		db.insert(TABLE_CUSTOM, null, values);
		db.close(); // Closing database connection
	}
	
	// Getting single exercise
	public int getCount() {
    	// Select All Query
    	String selectQuery = "SELECT * FROM " + TABLE_CUSTOM;
    	
    	SQLiteDatabase db = this.open();
    	Cursor cursor = db.rawQuery(selectQuery, null);
    	
    		
    	// looping through all rows and adding to list
    	int count= 0;
    	if (cursor.moveToFirst()) {
    			do {
    				count ++;
    		} while (cursor.moveToNext());
    	}
    	db.close();
        cursor.close();
        return count;
    }
	
	
	// Getting All Custom WODs
	public ArrayList<CustomWod> getAllCustomWods() {
    	Log.d("custom Wod", "its getting to here");
		ArrayList<CustomWod> customWodList = new ArrayList<CustomWod>();
    	// Select All Query
    	String selectQuery = "SELECT * FROM " + TABLE_CUSTOM;
    	
    	SQLiteDatabase db = this.open();
    	Cursor cursor = db.rawQuery(selectQuery, null);
    	
    		
    	// looping through all rows and adding to list
    	CustomWod customWod;
    	if (cursor.moveToFirst()) {
    	
    		do {
    			
    			customWod= new CustomWod( cursor.getString(1), cursor.getString(2), 
    					cursor.getString(3));
    			// Adding contact to list
    			customWodList.add(customWod);
    		} while (cursor.moveToNext());
    	}
    db.close();
    cursor.close();
    // return contact list
   	return customWodList;
    } 
	
	public void printAllCustomWods() {
		
    	// Select All Query
    	String selectQuery = "SELECT * FROM " + TABLE_CUSTOM;
    	
    	SQLiteDatabase db = this.open();
    	Cursor cursor = db.rawQuery(selectQuery, null);
    	
    		
    	// looping through all rows and adding to list
    	
    	if (cursor.moveToFirst()) {
    		
    		do {
    			
    			Log.d("CustomWods", cursor.getString(1) + cursor.getString(2)
    				+ cursor.getString(3));
    			
    		} while (cursor.moveToNext());
    	}
    db.close();
    
   	
    }
	


}
