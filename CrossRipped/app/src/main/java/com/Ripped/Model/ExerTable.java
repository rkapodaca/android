package com.Ripped.Model;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ExerTable extends DBadapter {
	
	public ExerTable(Context context) {
		super(context);
	}
	
	
	// Adding new exercise
	public static void addInitialExercise(SQLiteDatabase db, String exName, 
			int wFilt, int eFilt, int uFilt, int lowFilt, int largeFilt) {

		ContentValues values = new ContentValues();
		values.put(KEY_EXER_NAME, exName); 
		values.put(KEY_WEIGHTS_FILT, wFilt);
		values.put(KEY_EQUIPNEED_FILT, eFilt);
		values.put(KEY_UPPERBODY_FILT, uFilt);
		values.put(KEY_LOWERBODY_FILT, lowFilt);
		values.put(KEY_LARGEAREA_FILT, largeFilt); 
			
		db.insert(TABLE_EXERCISES, null, values);

		
	}
	
	public void addExercise(String exName, int wFilt, int eFilt, int uFilt, int lowFilt, int largeFilt) {
		SQLiteDatabase db = this.open();

		ContentValues values = new ContentValues();
		values.put(KEY_EXER_NAME, exName); 
		values.put(KEY_WEIGHTS_FILT, wFilt);
	 
		values.put(KEY_EQUIPNEED_FILT, eFilt);
		values.put(KEY_UPPERBODY_FILT, uFilt);
		values.put(KEY_LOWERBODY_FILT, lowFilt);
		values.put(KEY_LARGEAREA_FILT, largeFilt); 
			
		db.insert(TABLE_EXERCISES, null, values);

		db.close(); // Closing database connection
	}
	
	// Returns cursor to specific exercises based on their filters
	public String [] getExercise(int wFilt, int eFilt, int uFilt, int lowFilt, int largeFilt) {
		
	    String findExer = queryString(wFilt, eFilt, uFilt, lowFilt, largeFilt);
		
	    SQLiteDatabase db = this.open();
		
		Cursor cursor = db.rawQuery(findExer, null);	
		cursor.moveToNext();	
		
		String[] exers = new String [cursor.getCount()];
		for(int i = 0; i < cursor.getCount(); i++)
		{
			exers[i] = cursor.getString(0);
			cursor.moveToNext();
		}
		
		cursor.close();
		db.close(); // Closing database connection
		return exers;
	}
	
	private String queryString(int wf, int ef, int uf, int lowf, int largef )
	{
		String query;
		
		if (wf == 0 && ef == 0 && uf == 0 && lowf == 0 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 0 && ef == 0 && uf == 0 && lowf == 0 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_EQUIPNEED_FILT + " == " + ef;
		else if (wf == 1 && ef == 0 && uf == 0 && lowf == 1 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_LOWERBODY_FILT + " == " + lowf + " AND "
				+ KEY_EQUIPNEED_FILT + " == " + ef;
		else if (wf == 0 && ef == 0 && uf == 0 && lowf == 1 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_LOWERBODY_FILT + " == " + lowf + " AND "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 0 && ef == 0 && uf == 0 && lowf == 1 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES +" where "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_LOWERBODY_FILT + " == " + lowf;	
		else if (wf == 0 && ef == 0 && uf == 1 && lowf == 0 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_UPPERBODY_FILT + " == " + uf + " AND "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 0 && ef == 0 && uf == 1 && lowf == 0 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES +" where "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_UPPERBODY_FILT + " == " + uf;
		else if (wf == 0 && ef == 0 && uf == 1 && lowf == 1 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES +" where "
				+ KEY_LOWERBODY_FILT + " == " + lowf + " AND "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef + " AND "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " OR "
				+ KEY_UPPERBODY_FILT + " == " + uf + " AND "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef + " AND "
				+ KEY_EQUIPNEED_FILT + " == " + ef;	
		else if (wf == 0 && ef == 0 && uf == 1 && lowf == 1 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES +" where "
				+ KEY_LOWERBODY_FILT + " == " + lowf + " AND "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " OR "
				+ KEY_UPPERBODY_FILT + " == " + uf + " AND "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_EQUIPNEED_FILT + " == " + ef;	
		else if (wf == 0 && ef == 1 && uf == 0 && lowf == 0 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 0 && ef == 1 && uf == 0 && lowf == 0 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_WEIGHTS_FILT + " == " + wf;
		else if (wf == 0 && ef == 1 && uf == 0 && lowf == 1 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_LOWERBODY_FILT + " == " + lowf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 0 && ef == 1 && uf == 0 && lowf == 1 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES +" where "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_LOWERBODY_FILT + " == " + lowf;
		else if (wf == 0 && ef == 1 && uf == 1 && lowf == 0 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES +" where "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_UPPERBODY_FILT + " == " + uf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 0 && ef == 1 && uf == 1 && lowf == 0 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES +" where "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_UPPERBODY_FILT + " == " + uf;
		else if (wf == 0 && ef == 1 && uf == 1 && lowf == 1 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES +" where "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_UPPERBODY_FILT + " == " + uf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef + " OR "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_LOWERBODY_FILT + " == " + lowf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 0 && ef == 1 && uf == 1 && lowf == 1 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES +" where "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_UPPERBODY_FILT + " == " + uf + " OR "
				+ KEY_WEIGHTS_FILT + " == " + wf + " AND "
				+ KEY_LOWERBODY_FILT + " == " + lowf;
		else if (wf == 1 && ef == 0 && uf == 0 && lowf == 0 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 1 && ef == 0 && uf == 0 && lowf == 0 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_EQUIPNEED_FILT + " == " + ef;
		else if (wf == 1 && ef == 0 && uf == 0 && lowf == 1 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_LOWERBODY_FILT + " == " + lowf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 1 && ef == 0 && uf == 0 && lowf == 1 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_LOWERBODY_FILT + " == " + lowf;
		else if (wf == 1 && ef == 0 && uf == 1 && lowf == 0 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_UPPERBODY_FILT + " == " + uf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 1 && ef == 0 && uf == 1 && lowf == 0 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_UPPERBODY_FILT + " == " + uf;
		else if (wf == 1 && ef == 0 && uf == 1 && lowf == 1 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_UPPERBODY_FILT + " == " + uf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef + " OR "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_LOWERBODY_FILT + " == " + lowf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 1 && ef == 0 && uf == 1 && lowf == 1 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_UPPERBODY_FILT + " == " + uf + " OR "
				+ KEY_EQUIPNEED_FILT + " == " + ef + " AND "
				+ KEY_LOWERBODY_FILT + " == " + lowf;
		else if (wf == 1 && ef == 1 && uf == 0 && lowf == 0 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES +" where "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 1 && ef == 1 && uf == 0 && lowf == 0 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES;
		else if (wf == 1 && ef == 1 && uf == 0 && lowf == 1 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_LOWERBODY_FILT + " == " + lowf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 1 && ef == 1 && uf == 0 && lowf == 1 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES +" where "
				+ KEY_LOWERBODY_FILT + " == " + lowf;
		else if (wf == 1 && ef == 1 && uf == 1 && lowf == 0 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_UPPERBODY_FILT + " == " + uf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 1 && ef == 1 && uf == 1 && lowf == 0 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES +" where "
				+ KEY_UPPERBODY_FILT + " == " + uf;	
		else if (wf == 1 && ef == 1 && uf == 1 && lowf == 1 && largef == 0)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_UPPERBODY_FILT + " == " + uf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef + " OR "
				+ KEY_LOWERBODY_FILT + " == " + lowf + " AND "
				+ KEY_LARGEAREA_FILT + " == " + largef;
		else if (wf == 1 && ef == 1 && uf == 1 && lowf == 1 && largef == 1)
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES + " where "
				+ KEY_UPPERBODY_FILT + " == " + uf + " OR "
				+ KEY_LOWERBODY_FILT + " == " + lowf;
		else 
			query = "SELECT exername FROM "
				+ TABLE_EXERCISES;
		return query;
	}
	
	// Getting All Exercises 
	public List<String> getAllExercies() {
		List<String> exerciseList = new ArrayList<String>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_EXERCISES;
		
		SQLiteDatabase db = this.open();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				exerciseList.add(cursor.getString(1));
			} while (cursor.moveToNext());
		}
	db.close();
	// return contact list
	return exerciseList;
	}
	
	public void printAllExercies() {
		
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_EXERCISES;
		
		SQLiteDatabase db = this.open();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Log.d("Exercise:",cursor.getString(1));
			} while (cursor.moveToNext());
		}
	db.close();
	// return contact list;
	}

	
}
