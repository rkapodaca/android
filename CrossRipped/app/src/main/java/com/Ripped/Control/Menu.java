package com.Ripped.Control;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.EditText;

import com.Ripped.CrossRipped.AboutMenu;
import com.Ripped.CrossRipped.DietNutritionMenu;
import com.Ripped.CrossRipped.HelpMenu;
import com.Ripped.CrossRipped.LiftTechMenu;
import com.Ripped.CrossRipped.MainMenu;
import com.Ripped.CrossRipped.OlympicLogMenu;
import com.Ripped.CrossRipped.OptionMenu;
import com.Ripped.CrossRipped.PowerLogMenu;
import com.Ripped.CrossRipped.QnAMenu;
import com.Ripped.CrossRipped.R;
import com.Ripped.CrossRipped.TrackerMenu;
import com.Ripped.CrossRipped.UserInMenu;
import com.Ripped.CrossRipped.WodArchiveCustomMenu;
import com.Ripped.CrossRipped.WodArchiveGirlsMenu;
import com.Ripped.CrossRipped.WodArchiveHerosMenu;
import com.Ripped.CrossRipped.WodGeneratorMenu;
import com.Ripped.CrossRipped.WodMenu;
import com.Ripped.Model.CustomWodTable;
import com.Ripped.Model.DBadapter;

public abstract class Menu extends Activity implements OnItemSelectedListener{
	
	private String logWorkoutName = " ";
	private static int counter;
	private String typeWod = "";
	private String exe = "";
	private int logIdButton = -1;
	
	public void clickHandler(View v) {
		switch(v.getId()){
			
			case R.id.trackerButton:
				startActivity(new Intent(this, TrackerMenu.class));
				break;
				
			case R.id.officialButton:
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com/gwt/n?u=crossfit.com/pdawod.html&_gwt_noimg=1")));
				break;

			/*case R.id.qnaButton:
				startActivity(new Intent(this, QnAMenu.class));
				break;*/
				
			case R.id.wodButton:
				startActivity(new Intent(this, WodMenu.class));
				break;
	
			case R.id.optionsButton:
				startActivity(new Intent(this, OptionMenu.class));
				break;
	
			case R.id.aboutButton:
				startActivity(new Intent(this, AboutMenu.class));
				break;
	
			case R.id.helpButton:
				startActivity(new Intent(this, HelpMenu.class));
				break;
				
			case R.id.liftTechButton:
				startActivity(new Intent(this, LiftTechMenu.class));
				break;
	
			case R.id.homeButton:
				startActivity(new Intent(this, MainMenu.class));
				break;
	
			case R.id.userInButton:
				startActivity(new Intent(this, UserInMenu.class));
				break;
		
			case R.id.dietButton:
				startActivity(new Intent( this.getApplicationContext(), DietNutritionMenu.class));
				break;
				
			case R.id.olympicLiftsButton:
				alertTracker(R.id.olympicLiftsButton);
				break;
	
			case R.id.powerLiftsButton:
				alertTracker(R.id.powerLiftsButton);
				break;
	
			case R.id.wodarchiveButton:
				startArchive();
				break;
	
			case R.id.backFromWodGenButton:
				startActivity(new Intent(this, WodMenu.class));
				break;
				
			case R.id.backFromList:
				startActivity(new Intent(this, WodMenu.class));
				break;
	
			case R.id.backFromUserInfoButton:
				startActivity(new Intent(this, TrackerMenu.class));
				break;
	
			case R.id.wodgeneratorButton:
				startActivity(new Intent(this, WodGeneratorMenu.class));
				break;
				
			case R.id.videoButton:
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=EvScGIhX43Q")));
				break;
	
			case R.id.generateButton:
				generateWod();	
				break;
			
			case R.id.logButton:
				recordLift();
				break;
				
			case R.id.enteredweight:
				((EditText) findViewById(R.id.enteredweight)).getText().clear();
				
			case R.id.backFromLogBook:
				startActivity(new Intent(this, TrackerMenu.class));
				break;
				
			case R.id.resetButton:
				alertReset();
				break;
				
			case R.id.girlsButton:
				startActivity(new Intent(this, WodArchiveGirlsMenu.class));
				break;
				
			case R.id.heroesButton:
				startActivity(new Intent(this, WodArchiveHerosMenu.class));
				break;
			}

		
		
	}


	private void startArchive() {
		if(new CustomWodTable(this).getCount() != 0)
			startActivity(new Intent(this, WodArchiveCustomMenu.class));
		else{
			
			final AlertDialog error = new AlertDialog.Builder(this).create();
			error.setTitle("No Custom Wods to Display ");
			error.setMessage("Please save a custom generated wod before viewing the WodArchive");
		
			error.setButton("Ok", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {

					error.dismiss();
					
				} });
			error.setIcon(R.drawable.logo_r);
			error.show();
			
		
		}
	}


	private void generateWod()
	{
		boolean hasExercise = WodGen.generate(this, ((CheckBox)(findViewById(R.id.weights))).isChecked(),
				((CheckBox)(findViewById(R.id.no_equipment))).isChecked(),
				((CheckBox)(findViewById(R.id.upper_body))).isChecked(),
				((CheckBox)(findViewById(R.id.lower_body))).isChecked(),
				((CheckBox)(findViewById(R.id.widearea))).isChecked());
		final CustomWodTable cw = new CustomWodTable(this);
				
		
				ArrayList<String> customWodList;
				if(!hasExercise)
					typeWod = typeWod + "No WOD compatible to selected filters";
				else{
					customWodList = WodGen.getWOD();
					exe = customWodList.get(0);
					typeWod = customWodList.get(1);
					
					//CustomWodTable cw = new CustomWodTable(this);
					cw.printAllCustomWods();
				}
			        
				
				AlertDialog alertDialog = new AlertDialog.Builder(this).create();
				alertDialog.setTitle("Keep or Re-Gen?");
				alertDialog.setMessage("Here is your WOD: \n\n" +typeWod + exe);
				Log.d("Menu", ""+exe+"              "+typeWod);
				
				
				alertDialog.setButton("Keep WOD", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
						counter = cw.getCount();
						counter++;
						cw.addCustomWod("custom " + counter, exe, typeWod);
						nextGenOption(id);
					} });
				
				alertDialog.setButton2("Regenerate WOD", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
						nextGenOption(id);
					} });
				
				alertDialog.setIcon(R.drawable.logo_r);
				alertDialog.show();
				
	}
	
	
	
	private void nextGenOption(int popUpButtonId){
		if(popUpButtonId ==  -1 ){
			startActivity(new Intent(this, WodArchiveCustomMenu.class));
		}
		else
		{
			generateWod();
		}
	}
	
	
	private void alertTracker(final int trackButtonId){
		AlertDialog trackerPopUp = new AlertDialog.Builder(this).create();
		logIdButton = trackButtonId;
		
		
		if( R.id.olympicLiftsButton == trackButtonId)
			trackerPopUp.setTitle("Olympic Lifts LogBook");
		else			
			trackerPopUp.setTitle("Power Lifts LogBook");
		
		trackerPopUp.setMessage("Choose to either log an excercise or review your progress");
	
	
	
		trackerPopUp.setButton("Add Input", new DialogInterface.OnClickListener() {
	
			public void onClick(DialogInterface dialog, int id) {
				
				showInputLog(trackButtonId);
			} });
	
		trackerPopUp.setButton2("View Chart", new DialogInterface.OnClickListener() {
	
			public void onClick(DialogInterface dialog, int id) {
				
				showGraphs(trackButtonId);
	
			} });
	
	
		trackerPopUp.setIcon(R.drawable.logo_r);
	
		trackerPopUp.show();
		
		}
	
	
	private void showInputLog(int id)
	{
		if( id == R.id.powerLiftsButton){
			startActivity(new Intent(this, PowerLogMenu.class));
		}
		else{			
			startActivity(new Intent(this, OlympicLogMenu.class));
		}
	}
	
	
	private void showGraphs(int id){
			Intent achartIntent;
			if( id == R.id.powerLiftsButton){
				achartIntent = new LiftChart().powerLifts(this);
				
			}
			else{			
				achartIntent = new LiftChart().olympicLifts(this);
			}
			
			if(achartIntent != null)
				startActivity(achartIntent);
			else{
				final AlertDialog error = new AlertDialog.Builder(this).create();
			
				error.setTitle("No Lifts to Chart ");
			error.setMessage("Please log a Lift before trying to chart.");
		
			error.setButton("Ok", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					error.dismiss();
				} });
			
			error.show();
			}
		}
	
	
	private void recordLift() {
		final EditText inputWeight = (EditText) findViewById(R.id.enteredweight);
		int weightToLog = 0;
		
		try{
			weightToLog = Integer.parseInt(inputWeight.getText().toString());
			if(weightToLog <= 0)
				throw new Exception();
		}
		
		catch(Exception e){
			final AlertDialog error = new AlertDialog.Builder(this).create();
			error.setTitle("Incorrect Input For Weight ");
			error.setMessage("Please insert only positive whole numbers and try again");
		
			error.setButton("Ok", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					inputWeight.getText().clear();
					error.dismiss();
					
				} });
			error.setIcon(R.drawable.logo_r);
			error.show();
			return;
		}
		
		final AlertDialog success = new AlertDialog.Builder(this).create();
		success.setTitle("Success ");
		success.setMessage("You are logging: " +logWorkoutName + " " + weightToLog +
				"lbs.\nIs this correct?");
		
		final int weight = weightToLog;
		success.setButton("Yes", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int id) {
				
				inputWeight.getText().clear();
				success.dismiss();
				LiftLog.logWorkout(logWorkoutName, weight, getCtx());
				startActivity(new Intent(getCtx() , TrackerMenu.class));
			} });
		
		success.setButton2("Cancel", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int id) {
				
				inputWeight.getText().clear();
				success.dismiss();
				return;
				
			} });
		success.setIcon(R.drawable.logo_r);
		success.show();
		
		
	}
	
	
	public void onItemSelected(AdapterView<?> parent, View v, int pos,
			long id) {
		logWorkoutName = parent.getItemAtPosition(pos).toString();
				
	}

	
	public void onNothingSelected(AdapterView<?> parent) {
		// do nothing
	}
	
	private Context getCtx()
	{
		return this;
	}
	
	
	private void alertReset(){
		
		final DBadapter db= new DBadapter(this);
       
		final AlertDialog resetAlert = new AlertDialog.Builder(this).create();
		resetAlert.setTitle("Reset to Default");
		resetAlert.setMessage("Warning everything will be reset back to factory settings");
		
		
		resetAlert.setButton("Reset", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int id) {
				
				db.onReset(db.open());
				db.close();
				//resetAlert.dismiss();
			} });
		
		resetAlert.setButton2("Cancel", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int id) {
				//resetAlert.dismiss();
				startActivity(new Intent(getCtx(), OptionMenu.class));
			} });
		
		resetAlert.setIcon(R.drawable.logo_r);
		resetAlert.show();
	
		
	}

}