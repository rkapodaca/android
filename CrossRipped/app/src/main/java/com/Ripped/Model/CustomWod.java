package com.Ripped.Model;

public class CustomWod {
	private String name;
	private String instruction;
	private String time;
	
	public CustomWod(String n,String i, String ti)
	{
		name = n;
		instruction = i;
		time = ti;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	public String getInstruction() {
		return instruction;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getTime() {
		return time;
	}
}
