/**
 * MainActivity.java
 * Driver Activity that will act as the intermediary to display the main screen
 * Will show the main home screen
 * 
 * @author - Ruben Apodaca, Kevin Ngo
 **/

package org.kp.sdcatapp2Droid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.kp.sdcatapp2Droid.R;
import org.kp.sdcatapp2Droid.db.Event;
import org.kp.sdcatapp2Droid.db.PrepopulatedData;

import com.pushio.manager.PushIOManager;
import com.pushio.manager.tasks.PushIOListener;

import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.ExpandableListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;


@TargetApi(11)
public class MainActivity extends Activity implements PushIOListener {
	
	private PushIOManager mPushIOManager;
	private ProgressDialog mProgressDialog;

	private static SparseArray<Event> mEventMap;
	private static ArrayList<Event> todaysEvents;
	private static ArrayList<Event> timelineEvents;
	
	private TextView patientName, patientDOB, patientReason, patientNurse,
			patientEvent;
	private ListView todayEvents, pastEvents;
	private Button callNurseBtn, feedbackBtn;
	private AlarmManager aManager;
	private ExpandableListView upEventsList, pastEventsList;
	private static ExpandableListAdapter mAdapter;
	
	private static final String HEADER = "HEADER";
	

	public static Event lastClickEvent = null;
	
	public static Event debugTest = null;
	
	    /** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState)
	    {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_main);
	     
	        mPushIOManager = new PushIOManager(getApplicationContext(), this, null);
	        mPushIOManager.ensureRegistration();
	       
	        // Grabs current prepopulated data
	        PrepopulatedData.prepopulateData();
	        todaysEvents = PrepopulatedData.getTodaysEvents();
	        timelineEvents = PrepopulatedData.getPastEvents();
	        
	        // View Initializations
	        this.aManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
	        this.setupViews();
	        
	        IconListAdapter todayAdapter = new IconListAdapter(getApplicationContext());
	        IconListAdapter pastAdapter = new IconListAdapter(getApplicationContext());
	        todayEvents = (ListView)findViewById(R.id.todayList);
	        pastEvents = (ListView)findViewById(R.id.pastList);
	       
	        // Populates todayEvent list
	        mEventMap = new SparseArray<Event>();
	        todayEvents.setAdapter(todayAdapter);
	        int total = 0;
	        for (int i = 0; i < todaysEvents.size(); i++)
	        {
	        	todayAdapter.add("\n" + todaysEvents.get(i).getName() + " " + todaysEvents.get(i).getTimeStr()
	        			+ " (" + todaysEvents.get(i).getDuration() +")\n" + "#" + total);
	        	mEventMap.append(total, todaysEvents.get(i));
	        	total++;
	        }
	        
	        // Populates pastEvent list
	        pastEvents.setAdapter(pastAdapter);
	        for (int i = 0; i < timelineEvents.size(); i++)
	        {
	        	pastAdapter.add("\n" + timelineEvents.get(i).getName() + " " + timelineEvents.get(i).getTimeStr()
	        			+ " (" + timelineEvents.get(i).getDuration() +")\n" + "#" + total);
	        	mEventMap.put(total, timelineEvents.get(i));
	        	total++;
	        }
	        
	        // Constructs tab list structure in Activity
	        TabHost tabs = (TabHost)findViewById(R.id.TabHost01);
	        tabs.setup();
	        
	        TabHost.TabSpec spec1 = tabs.newTabSpec("Corgi");
	        
	        spec1.setContent(R.id.todayList);
	        spec1.setIndicator("Upcoming Events (2)");
	        
	        tabs.addTab(spec1);
	        
	        TabHost.TabSpec spec2 = tabs.newTabSpec("Corgi3");
	        spec2.setContent(R.id.pastList);
	        spec2.setIndicator("Timeline (21)");
	        
	        tabs.addTab(spec2);
	        

	        //Button lButton = (Button) this.findViewById(R.id.clear_register_test_cats);
	        //
	        /*lButton.setOnClickListener( new View.OnClickListener() {
	            @Override
	            public void onClick(View view) {
	                showProgress();
	                List<String> lTags = new ArrayList<String>();
	                lTags.add("test1");
	                lTags.add("test2");
	                mPushIOManager.registerCategories(lTags, true );
	            }
	        });

	     */

	        
	        
	        todayEvents.setOnItemClickListener(new OnItemClickListener(){
	        	public void onItemClick(AdapterView<?> parent, View view, int position, long id){
	        		
	        		
	        		AlertDialog.Builder todayListPopUp = new AlertDialog.Builder(MainActivity.this);
		        	
		        	final Event single_event = todaysEvents.get(position);
		        	lastClickEvent = single_event; 
		        	todayListPopUp.setMessage(single_event.getName());
		        	todayListPopUp.setTitle("Envent info");

					
					
					todayListPopUp.setNegativeButton("Request", new DialogInterface.OnClickListener() {
				
						public void onClick(DialogInterface dialog, int id) {
							Intent intent = new Intent(getApplicationContext(), RequestActivity.class);
		                    startActivity(intent); 
						} });
				
					todayListPopUp.setPositiveButton("Notes", new DialogInterface.OnClickListener() {
				
						public void onClick(DialogInterface dialog, int id) {
							Intent intent = new Intent(getApplicationContext(), NotesActivity.class);
		                    startActivity(intent); 
						}});
					
					
					todayListPopUp.setNeutralButton("Notify", new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int id) {
							 new SendPush().execute(single_event.getName(), single_event.getTimeStr());
							 dialog.cancel();
							 Toast.makeText(getApplicationContext(), "Push Notification Sent", Toast.LENGTH_SHORT).show();
						} });
					
					
					
					AlertDialog alert = todayListPopUp.create();
					alert.show();
	
	        		
	        	}

		
	        });
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        pastEvents.setOnItemClickListener(new OnItemClickListener(){
	        	public void onItemClick(AdapterView<?> parent, View view, int position, long id){
	        		
	        		
	        		AlertDialog.Builder pastListPopUp = new AlertDialog.Builder(MainActivity.this);
		        	
		        	Event single_event = timelineEvents.get(position);
		        	lastClickEvent = single_event; 
		        	String feedback = "";
		        	
		        	if(single_event.getFeedback() == null)
		        		feedback = "Rating:\nRating Details:";
		        	else
		        		feedback = "Rating: " + single_event.getFeedback().getRate() + 
		        		"\nRating Details: " + single_event.getFeedback().getDetails();
		        	pastListPopUp.setMessage(single_event.getName() +"\nDate: " + single_event.getDateStr() + "\nNotes: "+ single_event.getNotes() +"\n" + feedback);
		        	pastListPopUp.setTitle("Past Events info");

					
					
					pastListPopUp.setNegativeButton("Rate", new DialogInterface.OnClickListener() {
				
						public void onClick(DialogInterface dialog, int id) {
							Intent intent = new Intent(getApplicationContext(), FeedbackActivity.class);
		                    startActivity(intent); 
						} });
				
					pastListPopUp.setPositiveButton("Notes", new DialogInterface.OnClickListener() {
				
						public void onClick(DialogInterface dialog, int id) {
							Intent intent = new Intent(getApplicationContext(), NotesActivity.class);
		                    startActivity(intent); 
						}});
					
					
					
					
					AlertDialog alert = pastListPopUp.create();
					alert.show();
	
	        		
	        	}

		
	        });
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	    }



				

	        	
			
			
				//todayListPopUp.setIcon(R.drawable.);
		
	    
	    class SendPush extends AsyncTask<String, Void, ArrayList<String>> {    

	 	     
	    	@Override     
	    	protected void onPreExecute() {                 
	    		super.onPreExecute();  
	    		}      
	    	@Override     
	    	protected   ArrayList<String> doInBackground(String... params) {       		
	    		// Initialize array adapters.
	    		System.out.println("do in background");

	  		HttpParams httpParameters = new BasicHttpParams(); 
           	// Set the timeout in milliseconds until a connection is established.
           	HttpConnectionParams.setConnectionTimeout(httpParameters, 10000); 
           	// Set the default socket timeout (SO_TIMEOUT) which is the timeout for waiting for data. 
           	HttpConnectionParams.setSoTimeout(httpParameters, 10000);  
	            HttpClient httpclient = new DefaultHttpClient(httpParameters);   
	            String responseString = "";
	            
	            try {
	            	 HttpPost httppost = new HttpPost("https://manage.push.io/api/v1/notify_app/iTA0x5yjlg/No9kxd5exc7yG2n1tsbM");
	            	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);  
	            	nameValuePairs.add(new BasicNameValuePair("payload", "{\"message\": \"" + "Kiser Event: " + params[0] + " " + params[1] + "\"}"));
	            	nameValuePairs.add(new BasicNameValuePair("audience", "broadcast"));
	            	httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs)); 
	            	 HttpResponse response = httpclient.execute(httppost);  

	                	StatusLine statusLine = response.getStatusLine();  
	            
	            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
	            	System.out.println("ok if");

	            	}
	            } catch (ConnectTimeoutException e) {
	            	
	            
  	            } catch (Exception e){
	            	//throw new IOException(statusLine.getReasonPhrase());
	            	System.out.println(e);
	            }

	  
	    		return null;

	    		}     
	    	@Override     
	    	protected void onPostExecute(  ArrayList<String> result) {         
	    		super.onPostExecute(result);   

	    		} 
	    	}
	    
	    



	    
	    
	    /**
	     * Sets up the views of the Activity
	     * 
	     * @author Kevin 
	     **/
	    private void setupViews()
	    {
	    	patientName = (TextView)findViewById(R.id.tv_patientName);
	    	patientDOB = (TextView)findViewById(R.id.tv_dob);
	    	patientReason = (TextView)findViewById(R.id.tv_reason);
	    	patientNurse = (TextView)findViewById(R.id.tv_nurseDuty);
	    	patientEvent = (TextView)findViewById(R.id.tv_upcomingEvent);
	    	
	    	callNurseBtn = (Button)findViewById(R.id.btn_nurse);
	    	feedbackBtn = (Button)findViewById(R.id.btn_feedback);
	    	
	    	feedbackBtn.setOnClickListener(new View.OnClickListener() {
				
				public void onClick(View view) {
					Intent intent = new Intent(getApplicationContext(), FeedbackActivity.class);
                    startActivity(intent); 
					
				}
			});

	    	//upEventsList = (ExpandableListView)findViewById(R.id.upEvents_expandList);
	    	//pastEventsList = (ExpandableListView)findViewById(R.id.pastEvents_expandList);

	    }
	   	    
	    @Override
	    protected void onStop()
	    {
	        super.onStop();
	        mPushIOManager.resetEID();
	    }

	    private void showProgress()
	    {
	        mProgressDialog = new ProgressDialog(this).show(this, "Talking to push.io!", "Just a sec. We're saving your push.io settings.", true );
	    }
	    
	    public void onPushIOError(String aMessage) 
	    {
	        mProgressDialog.dismiss();
	        new AlertDialog.Builder(this).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	            
	            public void onClick(DialogInterface dialogInterface, int i) {
	                mProgressDialog.dismiss();
	            }
	        }).setTitle("Whoa!").setMessage( aMessage).show();

	        mPushIOManager = new PushIOManager(getApplicationContext(), this, null);
	        mPushIOManager.ensureRegistration();
	        
	        /*Button lButton = (Button) this.findViewById(R.id.clear_register_test_cats);
	        lButton.setOnClickListener( new View.OnClickListener() {
	            @Override
	            public void onClick(View view) {
	               
	                List<String> lTags = new ArrayList<String>();
	                lTags.add("test1");
	                lTags.add("test2");
	                mPushIOManager.registerCategories(lTags, true );
	            }
	        });*/
}

	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) 	
	    {
	    	getMenuInflater().inflate(R.menu.activity_main, menu);
	    	return true;
	    }


	    
	    public void onPushIOSuccess() 
	    {
	    	// TODO Auto-generated method stub
		
	    }
	    
	    public static class IconListAdapter extends ArrayAdapter<String> {
	    	
            private final LayoutInflater mInflater;
            
            
            public IconListAdapter(Context context) {
                super(context, R.layout.list_layout);
                mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

            public void setData(ArrayList<String> data) {
                clear();
                if (data != null) {
                    addAll(data);
                }
            }

            /**
             * Populate new items in the list.
             */
            @Override public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                if (convertView == null) {
                    view = mInflater.inflate(R.layout.icon_text_layout, parent, false);
                } else {
                    view = convertView;
                }
                String[] split = getItem(position).split("#");
                ((TextView)view.findViewById(R.id.text)).setText(split[0]);
                int eType = mEventMap.get(Integer.parseInt(split[1])).getType();
                //((TextView)view.findViewById(R.id.text)).setText(split[0]);
                switch(eType) {
                case Event.CHECKING_IN_EVENT:
                case Event.CHECKING_OUT_EVENT:
                	//darkgreen
                	((ImageView)view.findViewById(R.id.icon)).setImageResource(R.drawable.icon_greendark);
                	break;
                case Event.MEAL_EVENT:
                	//green
                	((ImageView)view.findViewById(R.id.icon)).setImageResource(R.drawable.icon_green);
                	break;
                case Event.CHECK_UP_EVENT:
                case Event.DOCTOR_VISIT:
                	//orange
                	((ImageView)view.findViewById(R.id.icon)).setImageResource(R.drawable.icon_orange);
                	break;
                case Event.MEDICATION_EVENT:
                	//dark blue
                	((ImageView)view.findViewById(R.id.icon)).setImageResource(R.drawable.icon_bluedark);
                	break;
                case Event.BLOOD_TEST_EVENT:
                	//red
                	((ImageView)view.findViewById(R.id.icon)).setImageResource(R.drawable.icon_red);
                	break;
                case Event.VISITING_EVENT:
                	//yellow
                	((ImageView)view.findViewById(R.id.icon)).setImageResource(R.drawable.icon_yellow);
                	break;
                case Event.HYGIENE_EVENT:
                	//blue
                	((ImageView)view.findViewById(R.id.icon)).setImageResource(R.drawable.icon_blue);
                	break;
                case Event.SURGERY_EVENT:
                	//purple
                	((ImageView)view.findViewById(R.id.icon)).setImageResource(R.drawable.icon_purple);
                	break;
  
                }

                     	
              
                return view;
            }
        }
	    
}