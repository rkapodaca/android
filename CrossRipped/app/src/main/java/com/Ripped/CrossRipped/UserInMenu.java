package com.Ripped.CrossRipped;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.Ripped.Control.Menu;
import com.Ripped.Control.NumberPicker;
import com.Ripped.Control.NumberPickerW;
import com.Ripped.Control.NumberPickerWtwo;

public class UserInMenu extends Menu {
	  /** Called when the activity is first created. */
	 private NumberPicker h1;
	 private NumberPicker h2;
	 private NumberPickerW w1;
	 private NumberPicker w2;
     private TextView BMI;
    
    @Override
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userinfo); 
        
        h1 = (NumberPicker)findViewById(R.id.PickerHeight1);
        h2 = (NumberPicker)findViewById(R.id.PickerHeight2);
        w1 = (NumberPickerW)findViewById(R.id.PickerWeight1);
        w2 = (NumberPicker)findViewById(R.id.PickerWeight2);
        BMI = (TextView)findViewById(R.id.outHeight); 
        
         BMI.setTextColor(-1);
         BMI.setText("BMI: Enter Height And Weight");
         
  
           	}

    public void update(View v) {
    	
    	double bmi;
    	double height = (h1.getValue()  * 12) + (h2.getValue());
       	double weight = (double) new Double (w1.getValue() + "." + w2.getValue());
    	
       	final ImageButton update = (ImageButton)findViewById(R.id.updateButton);
       	
    	bmi = ( weight / ( height * height ) ) * 703;
    	
    	
    
    	
    	switch(v.getId()){	
	
		case R.id.updateButton:
			if (height == 0 || weight == 0){
				BMI.setText(String.format("BMI: Update Height and Weight"));
			}else {
				BMI.setText(String.format("BMI: %.2f", bmi));
			}
				
			break; 
			
			
    	

	}
}
}
